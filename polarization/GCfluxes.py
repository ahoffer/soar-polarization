"""Extract fluxes from globular cluster files"""

import glob


directory = 'E:/Science/OBS/2013-04-06/GCs/'
v0_list = glob.glob(directory+'phot*V0*.fits')
v1_list = glob.glob(directory+'phot*V1*.fits')
v3_list = glob.glob(directory+'phot*V3*.fits')
v5_list = glob.glob(directory+'phot*V5*.fits')



import lnr
import numpy as np
filename = 'E:/fluxmatch_ngc1851_R35.txt'
fileobj = open(filename, 'r')
filetest =  fileobj.read()


test = np.fromstring(filetest, dtype=float32, sep=',') # array from fluxmatch.txt from IDL pol_match.pro script
test = np.reshape(test, (-1,4))

vectors = filter(lambda x: all(x), test)
vectors = np.array(vectors)
x = vectors[:,0]
y = vectors[:,2]
xerr = vectors[:,1]
yerr = vectors[:,3]

lnrval = lnr.bces(x, y, x1err=xerr, x2err = yerr)
try:
	offset = np.vstack((offset,np.hstack((lnrval[0],filename))))
except:
	offset = np.array([0,0,''])
	offset = np.vstack((offset,np.hstack((lnrval[0],filename))))
	offset = offset[1:,:]
plt.errorbar(x,y,yerr=yerr,xerr=xerr, fmt='o')
plt.xlabel("Stellar Flux [Magnitudes]")
plt.ylabel("Stellar Flux [Magnitudes]")