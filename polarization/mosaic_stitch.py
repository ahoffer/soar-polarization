def mosaic_stitch(outfile=None,gaps=None**kwargs):
	"""Stitch multi-extention FITS (MEF) files together

	Keyword Arguments:
		kwargs -- Full file paths for list of files. Dictionary gives order of the extensions
		outfile -- Full file path for the mosaicked output file.
		gaps -- Add gaps in image coordinates between the image extensions.

	"""
	import numpy as np
	import scipy.stats as stats
	import pyfits

	#need to figure out how to kwargs it for any number of images
	for count in range(1,5): #currently set for the 4 extensions in SOAR images
		image_1 = pyfits.open(file_1)
		data1 = image_1[count].data # Make sure to grab the right FITS extension
		data1 = data1.astype('float')
		data1big = np.hstack((data1big,data1))
		exptime1 = image_1[0].header["EXPTIME"]



	return fits_mosaic