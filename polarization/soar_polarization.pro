pro soar_polarization

file = "/home/Aaron/External/External/SOAR/2011-01-07/mNGC1851V1.210.fits"
img = mrdfits(file, 0, header)
DEVICE, DECOMPOSED = 0
LOADCT,0 
dims = SIZE(img, /DIMENSIONS)  
padImg = REPLICATE(0B, dims[0]+10, dims[1]+10)  
padImg [5,5] = img  

dims = SIZE(padImg, /DIMENSIONS)  
WINDOW, 1, XSIZE = 2*dims[0], YSIZE = 2*dims[1], $  
    TITLE = 'Detecting Small Features with MORPH_TOPHAT'  
TVSCL, padImg, 0 
radius = 3  
strucElem = SHIFT(DIST(2*radius+1), radius, radius) LE radius 

tophatImg = MORPH_TOPHAT(padImg, strucElem)  
TVSCL, tophatImg, 1  

WINDOW, 2, XSIZE = 400, YSIZE = 300  
PLOT, HISTOGRAM(padImg)  

stretchImg = tophatImg < 70  
WSET, 0  
TVSCL, stretchImg, 2  
threshImg = tophatImg GE 60  
TVSCL, threshImg, 3  

stop

xc = 265
yc = 1684
skyrad = [20.,30.]
badpix = [0,0]
apr = 19.

aper, image,xc, yc, flux, eflux, sky, skyerr, 1, apr, skyrad, badpix, /FLUX, /NAN






end