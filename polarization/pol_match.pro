; match individual stars up to calibrate polarization
pro pol_match

root_dir = 'E:\Science\OBS\2013-01-04\GCs\'
file1 = root_dir+'testmM79_V3.286.fits'
file2 = root_dir+'testmM79_V5.287.fits'
out_file_histogram = root_dir+'M79_hist_V35_2.eps'
out_file_position = root_dir+'M79_pos_V35_2.eps'



a=''
x1 = dblarr(5000)
y1 = dblarr(5000) 
mag1 = dblarr(5000)

x2 = dblarr(5000)
y2 = dblarr(5000) 
mag2 = dblarr(5000)

i=0
close, 1
openr, 1, file1
while ~ EOF(1) do begin
  readf, 1, a
  skip = strpos(a, '#') ; skip the header
  if skip ne -1 then continue
  skip = strpos(a, 'm') 
    if skip ne 0 then begin ; skip ones with file name
      skip = strpos(a, '20.00') ;read the lines with the radius size as the first value
      if skip ne 3 then continue
      line = strsplit(a, " ", /EXTRACT)
      mag1(i-1) = line(4) 
      continue
    endif
  line = strsplit(a, " ", /EXTRACT)
  x1(i) = line(1)
  y1(i) = line(2) 
  i = i+1
endwhile

close, 1
x1 = x1(0:i-1)
y1 = y1(0:i-1)
mag1 = mag1(0:i-1)

j=0
close, 2
openr, 2, file2
while ~ EOF(2) do begin
  readf, 2, a
  skip = strpos(a, '#')
  if skip ne -1 then continue
  skip = strpos(a, 'm') ;skip ones that start with the filename
  if skip ne 0 then begin 
     skip = strpos(a, '20.00')
    if skip ne 3 then continue
    
    line = strsplit(a, " ", /EXTRACT)
    
    mag2(j-1) = line(4) 
    continue
  endif
  line = strsplit(a, " ", /EXTRACT)
  x2(j) = line(1)
  y2(j) = line(2) 
  j = j+1
endwhile
close, 2
x2 = x2(0:j-1)
y2 = y2(0:j-1)
mag2 = mag2(0:j-1)


x2match = dblarr(i)
y2match = dblarr(i)
bad = 0
positions = dblarr(i)
magdiff = dblarr(i)

for count = 0, i-1 do begin
  distance = sqrt((x1(count)-x2)^2. + (y1(count)-y2)^2.)
  if min(distance) gt 1 then begin
    bad = bad + 1
    ;print, bad
    positions(count) = -999
    magdiff(count) = -999
    continue
  endif
  
  x2match(count) = x2(where(distance eq min(distance)))
  y2match(count) = y2(where(distance eq min(distance)))
  positions(count) = where(distance eq min(distance))
  magdiff(count) = mag1(count) - mag2(where(distance eq min(distance)))
  
endfor  



magzero = 25. ;set in header
fluxratio = 10^(magdiff/2.5)
print, median(fluxratio(where(fluxratio ne 1.000)))



set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = out_file_histogram
;title = "mNGC1851V3.219 and mNGC1851V5.220"
nonzero = where(fluxratio ne 0 and fluxratio ne 1.0000) ; get rid of no values and bad values which give a ratio of exactly 1
fluxratio = fluxratio(nonzero)
histogauss, fluxratio, hist, xrange=[0.7,1.5]
;hist = histogram(fluxratio, binsize = .01, /NAN, max = 2, min = .5)
;plot, hist, title = title
;plothist, fluxratio, xrange = [.5,2], bin = .1
;histogram, fluxratio, min_value = .1, max_value = 2, NAN=nan  

device, /close

;plot the ratios as color coded circles
;bin first

len = size(nonzero)
len = len(1)
bin_len = round(len/5)
x_bin = congrid(x2match(nonzero), bin_len)
y_bin = congrid(y2match(nonzero), bin_len)
fluxratio_bin = congrid(fluxratio, bin_len)
set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = out_file_position

tvcircle, 100.0, (100.+x_bin)*5., (100.+y_bin)*5.,round(fluxratio_bin*100000),/DEVICE,/FILL

;create legend
tvcircle, 300.0, 12500.0, 5000.0, round(0.7*100000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 6000.0, round(0.8*100000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 7000.0, round(0.9*100000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 8000.0, round(1.0*100000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 9000.0, round(1.1*100000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 10000.0, round(1.2*100000),/DEVICE, /FILL
xyouts,13500.0, 4900.0, '0.7',/DEVICE
xyouts,13500.0, 5900.0, '0.8',/DEVICE
xyouts,13500.0, 6900.0, '0.9',/DEVICE
xyouts,13500.0, 7900.0, '1.0',/DEVICE
xyouts,13500.0, 8900.0, '1.1',/DEVICE
xyouts,13500.0, 9900.0, '1.2',/DEVICE
xyouts,11500.0, 10500.0, 'Flux Ratio Legend',/DEVICE
xyouts,1000.0, 12000.0, file1,/DEVICE
xyouts,1000.0, 11500.0, file2,/DEVICE

device, /close



stop


end









