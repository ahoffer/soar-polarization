;create polarization maps from 4 images 0,90,45,135


pro pol_maps

;set parameters of the observation here
prefix = "M87_Ha_big_tot"
halpha = 1 ; set to 1 if you want to subtract the continuum image from Halpha
in_root = 'G:\SOAR\April72011\'
file0 = in_root+'M87ON0.fits'
file1 = in_root+'M87ON1.fits'
file3 = in_root+'M87ON3.fits'
file5 = in_root+'M87ON5.fits'

out_reg ='G:\SOAR\April62011\analysis\'
reg_filename = out_reg+prefix+'polvector.reg'


;load 4 images

im0 = mrdfits(file0, 0, header0)
im1 = mrdfits(file1, 0, header1)
im3 = mrdfits(file3, 0, header3)
im5 = mrdfits(file5, 0, header5)

nimages = 1.0 ;number of images used to get file0, file1,... This information is in the header from imcombine: NCOMBINE

countfactor = nimages*2.0 ; gain 2 e-/DN

  mmm, im0, skymod0, ReadNoise=4.4
  mmm, im1, skymod1, ReadNoise=4.4
  mmm, im3, skymod3, ReadNoise=4.4
  mmm, im5, skymod5, ReadNoise=4.4
  
 
  
  im0_sub = im0 - skymod0
  im1_sub = im1 - skymod1
  im3_sub = im3 - skymod3
  im5_sub = im5 - skymod5




 ;set the binning dimensions
  im0_size = size(im0)
  binfactor = 1.0
  x_bin_size = round(im0_size(1)/binfactor)
  y_bin_size = round(im0_size(2)/binfactor)



im0_bin=congrid(im0_sub, x_bin_size,y_bin_size)
im1_bin=congrid(im1_sub, x_bin_size,y_bin_size)
im3_bin=congrid(im3_sub, x_bin_size,y_bin_size)
im5_bin=congrid(im5_sub, x_bin_size,y_bin_size)

im0=im0_bin*binfactor*countfactor
im1=im1_bin*binfactor*countfactor
im3=im3_bin*binfactor*countfactor
im5=im5_bin*binfactor*countfactor

exptime0 = fxpar(header0, "EXPTIME")
exptime1 = fxpar(header1, "EXPTIME")
exptime3 = fxpar(header3, "EXPTIME") 
exptime5 = fxpar(header5, "EXPTIME")

print, exptime0, exptime1,exptime3,exptime5 ; make sure all exposure times are the same

;otherwise scale all images to their exposure time 

    im0 = im0;/exptime0
    im1 = im1;/exptime1
    im3 = im3;/exptime3
    im5 = im5;/exptime5

  ;  im0 = sigma_filter(im0, 5, N_SIGMA = 5)
  ;  im1 = sigma_filter(im1, 5, N_SIGMA = 5)
  ;  im3 = sigma_filter(im3, 5, N_SIGMA = 5)
  ;  im5 = sigma_filter(im5, 5, N_SIGMA = 5)


  
  ; error image
  rd_noise = 4.4 ; 4.4 e-/pix
  im0_err = sqrt(im0+skymod0+rd_noise)
  im1_err = sqrt(im1+skymod1+rd_noise)
  im3_err = sqrt(im3+skymod3+rd_noise)
  im5_err = sqrt(im5+skymod5+rd_noise)
  

if halpha eq 1 then begin 
;for Halpha subtract the continuum component
  cont0= 'G:\SOAR\April72011\M87OFF0.fits'
  cont1= 'G:\SOAR\April72011\M87OFF1.fits'
  cont3= 'G:\SOAR\April72011\M87OFF3.fits'
  cont5= 'G:\SOAR\April72011\M87OFF5.fits'

  cont_im0= mrdfits(cont0, 0, contheader0)
  cont_im1= mrdfits(cont1, 0, contheader1)
  cont_im3= mrdfits(cont3, 0, contheader3)
  cont_im5= mrdfits(cont5, 0, contheader5)

  contexp0 = fxpar(header0, "EXPTIME")
  contexp1 = fxpar(header1, "EXPTIME")
  contexp3 = fxpar(header3, "EXPTIME")
  contexp5 = fxpar(header5, "EXPTIME")
  
  print, contexp0, contexp1, contexp3, contexp5
  
;  cont_im0 = sigma_filter(cont_im0, 5, N_SIGMA = 5,/MONITOR)
;    cont_im1 = sigma_filter(cont_im1, 5, N_SIGMA = 5)
;    cont_im3 = sigma_filter(cont_im3, 5, N_SIGMA = 5)
;    cont_im5 = sigma_filter(cont_im5, 5, N_SIGMA = 5)
  
  ;scale images based on exposure time
  cont_im0 = cont_im0;/contexp0
  cont_im1 = cont_im1;/contexp1
  cont_im3 = cont_im3;/contexp3
  cont_im5 = cont_im5;/contexp5
  
  
  mmm, cont_im0, skymod0_cont
  mmm, cont_im1, skymod1_cont
  mmm, cont_im3, skymod3_cont
  mmm, cont_im5, skymod5_cont
  
  cont_im0_sub = cont_im0 - skymod0_cont
  cont_im1_sub = cont_im1 - skymod1_cont
  cont_im3_sub = cont_im3 - skymod3_cont
  cont_im5_sub = cont_im5 - skymod5_cont

  corr_factor_0=median(cont_im0_sub)/median(im0_sub)
  corr_factor_1=median(cont_im1_sub)/median(im1_sub)
  corr_factor_3=median(cont_im3_sub)/median(im3_sub)
  corr_factor_5=median(cont_im5_sub)/median(im5_sub)
  
  corr_factor=(corr_factor_0+corr_factor_1+corr_factor_3+corr_factor_5)/4.0

  im0 = im0_sub - cont_im0_sub/corr_factor
  im1 = im1_sub - cont_im1_sub/corr_factor
  im3 = im3_sub - cont_im3_sub/corr_factor
  im5 = im5_sub - cont_im5_sub/corr_factor

  ;error image
im0_bin=congrid(im0, x_bin_size,y_bin_size)
im1_bin=congrid(im1, x_bin_size,y_bin_size)
im3_bin=congrid(im3, x_bin_size,y_bin_size)
im5_bin=congrid(im5, x_bin_size,y_bin_size)

im0=im0_bin*binfactor*countfactor
im1=im1_bin*binfactor*countfactor
im3=im3_bin*binfactor*countfactor
im5=im5_bin*binfactor*countfactor

cont_im0_bin=congrid(cont_im0_sub,x_bin_size ,y_bin_size)
cont_im1_bin=congrid(cont_im1_sub, x_bin_size,y_bin_size)
cont_im3_bin=congrid(cont_im3_sub, x_bin_size,y_bin_size)
cont_im5_bin=congrid(cont_im5_sub, x_bin_size,y_bin_size)

cont_im0_sub=cont_im0_bin*binfactor*countfactor
cont_im1_sub=cont_im1_bin*binfactor*countfactor
cont_im3_sub=cont_im3_bin*binfactor*countfactor
cont_im5_sub=cont_im5_bin*binfactor*countfactor
  
im0_err = sqrt(im0+rd_noise+skymod0+cont_im0_sub/corr_factor+skymod0_cont/corr_factor)
im1_err = sqrt(im1+rd_noise+skymod1+cont_im1_sub/corr_factor+skymod1_cont/corr_factor)
im3_err = sqrt(im3+rd_noise+skymod3+cont_im3_sub/corr_factor+skymod3_cont/corr_factor)
im5_err = sqrt(im5+rd_noise+skymod5+cont_im5_sub/corr_factor+skymod5_cont/corr_factor)

;output continuum subtracted images

mwrfits, im0, out_reg + prefix + "_0_net.fits", header0
mwrfits, im1, out_reg + prefix + "_1_net.fits", header1
mwrfits, im3, out_reg + prefix + "_3_net.fits", header3
mwrfits, im5, out_reg + prefix + "_5_net.fits", header5




endif






corr = 1.01 ; Need to verify ;calibration factors 0-1 = 1.01, 3-5 = 0.93, 01 - 35 = 1.03

I01 = (im0 + im1)/2.
I35 = (im3 + im5)/2.
Iavg = (I01+corr*I35)/2.
Iavg1 = I01
Q = (im0-im1)/2.
U = (im3-im5)/2. ;NOTE: The sign of U is switched everything looks right
V = sqrt((Q^2.+(corr*U)^2.)/Iavg^2.)
max_V = where(V gt 1) ;can't have a fraction gt 1
if max_V(0) ne -1 then V(max_V) = 0
polangle = 0.5*atan(U/Q) 

;error images
I01_err = 0.5*sqrt(im0_err^2.+im1_err^2.)
I35_err = 0.5*sqrt((im3_err)^2.+im5_err^2.)
Iavg_err = 0.5*sqrt(I01_err^2.+(corr*I35_err)^2.)

Q_err = I01_err
U_err = I35_err

V_err = sqrt((Q*Q_err)^2.+(corr*U*U_err)^2.+(Iavg_err/Iavg)^2.)/(V*Iavg^2.)

Vunbias = ((Q^2.+U^2.)^0.5 - (Q_err^2 + U_err^2)^0.5)
Vunbias(where(Vunbias lt 0)) = 0
Vunbias = Vunbias/Iavg
Vunbias(where(Vunbias lt 0 or Vunbias gt 1)) = 0
Vunbias(where(V/V_err lt 5.0)) = 0


; convert the angles such that they are all between 0-180
neg1 = where(Q lt 0 and U gt 0) ; between 45 - 90
polangle(neg1) = polangle(neg1) + 3.14159/2.
neg2 = where(Q lt 0 and U lt 0) ; between 90 - 135
polangle(neg2) = polangle(neg2) + 3.14159/2.
neg3 = where(Q gt 0 and U lt 0) ; between 135 - 180
polangle(neg3) = polangle(neg3) + 3.14159

x_mag = V*cos(polangle) 
y_mag = V*sin(polangle)


badvals = where(x_mag ne x_mag)
 if(badvals(0) ne -1) then x_mag(badvals) = 0

badvals = where(y_mag ne y_mag)
if(badvals(0) ne -1) then y_mag(badvals) = 0

; bin arrays for plotting divided dimensions by factor
bin_x = congrid(x_mag, 100,100)
bin_y = congrid(y_mag, 100,100)

;set_plot, 'ps'
;device, filename = out_reg+'polimage_'+prefix+'.eps', /encapsulated, xsize = 18, ysize = 14, color = 1,decompose = 1, bits_per_pixel = 8
;ratio = alog10(U/Q)
;badvals = where(ratio ne ratio or ratio lt -10000 or ratio gt 10000)
;ratio(badvals) = -999

;overplot the polarization vectors on the intensity image
;tvscl, Iavg>3<50, xsize = 18, ysize = 14, /centimeters
;velovect, bin_x, bin_y, missing = .05, xmargin = [0,0], ymargin = [0,0], noclip = 1, /overplot, color = 255

;make a single vector to overplot

;device, /close


;create region file for the polarization vectors


close, 1
openw, 1, reg_filename 
printf, 1, '# Region file format: DS9 version 4.1'
printf, 1, '# Polarization vectors for '+prefix
printf, 1, 'global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1'
printf, 1, 'physical'
;printf, 1, '# vector(283,271,41.3401,212.152) vector=0' ;(headx,heady,length, angle)

;avg_v = 0
;avg_angle = 0
binnum = 5 ; set the density of polarization vectors in region file
;avg_count = 1

for xcount = 0, x_bin_size-1 do begin
  for ycount = 0, y_bin_size-1 do begin
    if finite(V(xcount,ycount)) eq 0 then continue
    if xcount mod binnum ne 0 then begin ; only include 1 region vector in binnum X binnum box
      ;avg_v = avg_v + V(xcount,ycount)
      ;avg_angle = avg_angle + polangle(xcount,ycount)
      ;avg_count = avg_count+1
      continue
    endif
    
    if ycount mod binnum ne 0 then begin
      ;avg_v = avg_v + V(xcount,ycount)
      ;avg_angle = avg_angle + polangle(xcount,ycount)
     ; avg_count = avg_count+1
      continue
    endif 
    if V(xcount,ycount)/V_err(xcount,ycount) lt 5.0 then continue ; S/N cutoff for vectors, only above XX sigma
    ;avg_v = (avg_v+V(xcount,ycount))/(avg_count)
    ;avg_angle = (avg_angle+polangle(xcount,ycount))/(avg_count)
    
    line = '# vector('+strcompress(xcount)+','+strcompress(ycount)+','+strcompress(8.0*binfactor*V(xcount,ycount))+','+strcompress(polangle(xcount,ycount)*180.0/3.1415926)+') vector=0'
    ;line = '# vector('+strcompress(xcount)+','+strcompress(ycount)+','+strcompress(4.0*binfactor*avg_v)+','+strcompress(avg_angle*180.0/3.1415926)+') vector=0'
    
    printf, 1, line
    ;avg_v = 0
    ;avg_angle = 0
    ;avg_count = 1
    endfor
endfor

close, 1



;set_plot, 'X'
Idiff = (I01 - I35)
Idiff = Idiff *finite(Idiff)
;plothist, Idiff, xrange = [-10, 10]

;print out all the polarization images

  ;Iavg = sigma_filter(Iavg, 20, N_SIGMA = 3,/MONITOR)

;create unsharp intensity image
sharpimage=unsharp_mask(Iavg)

mwrfits, Iavg, out_reg+prefix+'_I.fits'
mwrfits, I01, out_reg+prefix+'_I01.fits'
mwrfits, I35, out_reg+prefix+'_I35.fits'
mwrfits, Q, out_reg+prefix+'_Q.fits'
mwrfits, U, out_reg+prefix+'_U.fits'
mwrfits, V, out_reg+prefix+'_V.fits'
mwrfits, Vunbias, out_reg+prefix+'_Vunbias.fits'
mwrfits, V_err, out_reg+prefix+'_Verr.fits'
mwrfits, V/V_err, out_reg+prefix+'_SNmap.fits'
mwrfits, sharpimage, out_reg+prefix+'_unsharp.fits'

stop

end