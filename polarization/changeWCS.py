def changeWCS(pix1=None, pix2=None, val1=None, val2=None, crfileprefix = 'E:/Science/OBS/2013-01-04/', nameprefix = 'CN_V*sum.fits'):
	"""Change the WCS for SOI images using a single value from a 2MASS or other WCS calibrated source.

	Keyword Arguments:
		pix1 -- X pixel position point in image coordinates of the source you used to align
		pix2 -- Y pixel position point in image coordinates of the source you used to align
		val1 -- RA decimal degree value for equivalent point from 2MASS image
		val2 -- Dec decimal degree value for equivalent point from 2MASS image
		crfileprefix -- File folder for list
		nameprefix -- Prefix for list of names will use glob to search for all in the folder

	"""
	import pyfits
	import glob

	filelist = glob.glob(fileprefix+nameprefix+'*')
	
	if not pix1:
		pix1 = input("Please input value for pix1: ")
	if not pix2:
		pix2 = input("Please input value for pix2: ")
	if not val1:
		val1 = input("Please input value for val1: ")
	if not val2:
		val2 = input("Please input value for val2: ")

	for fitsfile in filelist:
		try:
			hdulist = pyfits.open(fitsfile, mode='update') # Update will automatically update the file when you close it.
			header = hdulist[0].header
			
			header['CRPIX1'] = pix1
			header['CRPIX2'] = pix2
			header['CRVAL1'] = val1
			header['CRVAL2'] = val2
	
			hdulist.close()
			print("WCS updated for "+fitsfile)
		except:
			print("Couldn't change file header for "+fitsfile)