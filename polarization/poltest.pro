;test the imarized star and polarized star to see how well we can reproduce the standards
pro poltest


;HD94851
im0 = mrdfits("H:\science\observing\soi\2011-12-27\mpolstarV0.230.fits",0,header0)
im1 = mrdfits("H:\science\observing\soi\2011-12-27\mpolstarV1.231.fits",0,header1)
im3 = mrdfits("H:\science\observing\soi\2011-12-27\mpolstarV3.232.fits",0,header3)
im5 = mrdfits("H:\science\observing\soi\2011-12-27\mpolstarV5.233.fits",0,header5)


exptime0 = fxpar(header0, "EXPTIME")
exptime1 = fxpar(header1, "EXPTIME")
exptime3 = fxpar(header3, "EXPTIME") 
exptime5 = fxpar(header5, "EXPTIME")

print, exptime0, exptime1,exptime3,exptime5 ; make sure all exposure times are the same
 ; mmm, im0, skymod0
 ; mmm, im1, skymod1
 ; mmm, im3, skymod3
 ; mmm, im5, skymod5
;print, skymod0, skymod1,skymod3,skymod5


coordx = 620.0
coordy = 1054.0
aperture = 15.0
back1 = 25.0
back2 = 50.0

aper, im0,coordx,coordy, mags0,errap,sky,skyerr, 1, aperture, [back1, back2], /NAN,/FLUX,/silent
aper, im1,coordx,coordy, mags1,errap,sky,skyerr, 1, aperture, [back1, back2], /NAN,/FLUX, /silent
aper, im3,coordx,coordy, mags3,errap,sky,skyerr, 1, aperture, [back1, back2], /NAN,/FLUX, /silent
aper, im5,coordx,coordy, mags5,errap,sky,skyerr, 1, aperture, [back1, back2], /NAN,/FLUX, /silent

;print, mags0, mags1, mags3,mags5

I01 = (mags0 + mags1)/2.
I35 = (mags3 + mags5)/2.
Iavg = (I01+I35)/2.
Iavg1 = I01
Q = (mags0-mags1)/2.
U = (mags3-mags5)/2. ;NOTE: The sign of U is switched everything looks right this depends on which direction they insert the filters
V = sqrt((Q^2.+(U)^2.)/Iavg^2.) ; V is polarization fraction
print, V
max_V = where(V gt 1) ;can't have a fraction gt 1
if max_V(0) ne -1 then V(max_V) = 0
polangle = 0.5*atan(U/Q) 


print, V,polangle*180.0/3.1415926
stop 
END