;stack polarizaition images
pro polflats

root_dir = 'E:\Science\OBS\2013-01-04\'
out_root = 'E:\Science\OBS\2013-01-04\test\test_'

im1prefix = 'Flat11'
im2prefix = 'Flat21'
im3prefix = 'Flat31'
im4prefix = 'Flat41'
im5prefix = 'Flat6563'
; only reading in one of the four images
im1 = make_array(2048,2048)
im2 = make_array(2048,2048)
im3 = make_array(2048,2048)
im4 = make_array(2048,2048)
im5 = make_array(2048,2048)

for i=1,4 do begin
  if i eq 1 then im1[0:511,0:2047] = mrdfits(root_dir+im1prefix+'.fits',i,header1)
  if i eq 2 then im1[512:1023,0:2047] = mrdfits(root_dir+im1prefix+'.fits',i,header1)
  if i eq 3 then im1[1024:1535,0:2047] = mrdfits(root_dir+im1prefix+'.fits',i,header1)
  if i eq 4 then im1[1536:2047,0:2047] = mrdfits(root_dir+im1prefix+'.fits',i,header1)
  
  print, header1[13]
endfor

for i=1,4 do begin
  if i eq 1 then im2[0:511,0:2047] = mrdfits(root_dir+im2prefix+'.fits',i,header2)
  if i eq 2 then im2[512:1023,0:2047] = mrdfits(root_dir+im2prefix+'.fits',i,header2)
  if i eq 3 then im2[1024:1535,0:2047] = mrdfits(root_dir+im2prefix+'.fits',i,header2)
  if i eq 4 then im2[1536:2047,0:2047] = mrdfits(root_dir+im2prefix+'.fits',i,header2)
  
  print, header2[13]
endfor
 
for i=1,4 do begin
  if i eq 1 then im3[0:511,0:2047] = mrdfits(root_dir+im3prefix+'.fits',i,header3)
  if i eq 2 then im3[512:1023,0:2047] = mrdfits(root_dir+im3prefix+'.fits',i,header3)
  if i eq 3 then im3[1024:1535,0:2047] = mrdfits(root_dir+im3prefix+'.fits',i,header3)
  if i eq 4 then im3[1536:2047,0:2047] = mrdfits(root_dir+im3prefix+'.fits',i,header3)
  
  print, header3[13]
endfor

for i=1,4 do begin
  if i eq 1 then im4[0:511,0:2047] = mrdfits(root_dir+im4prefix+'.fits',i,header4)
  if i eq 2 then im4[512:1023,0:2047] = mrdfits(root_dir+im4prefix+'.fits',i,header4)
  if i eq 3 then im4[1024:1535,0:2047] = mrdfits(root_dir+im4prefix+'.fits',i,header4)
  if i eq 4 then im4[1536:2047,0:2047] = mrdfits(root_dir+im4prefix+'.fits',i,header4)
  
  print, header4[13]
endfor

for i=1,4 do begin
  if i eq 1 then im5[0:511,0:2047] = mrdfits(root_dir+im5prefix+'.fits',i,header5)
  if i eq 2 then im5[512:1023,0:2047] = mrdfits(root_dir+im5prefix+'.fits',i,header5)
  if i eq 3 then im5[1024:1535,0:2047] = mrdfits(root_dir+im5prefix+'.fits',i,header5)
  if i eq 4 then im5[1536:2047,0:2047] = mrdfits(root_dir+im5prefix+'.fits',i,header5)
  
  print, header5[13]
endfor


;images created
thru_0 = im1/im5
thru_1 = im2/im5
thru_3 = im3/im5
thru_5 = im4/im5

pol01 = im1 + im2 
pol35 = im4 + im3

pol0135_diff = 0.5*(pol01+pol35)/pol01
pol0m1 = im2/thru_1 - im1/thru_0 
pol3m5 = im4/thru_5 - im3/thru_3

print, median(thru_0)
print, median(thru_1)
print, median(thru_3)
print, median(thru_5)

print, median(thru_0+thru_1), median(thru_3+thru_5)




mwrfits,pol01,out_root+'pol01.fits'
mwrfits,pol35,out_root+'pol35.fits'
mwrfits,pol0135_diff,out_root+'pol01-35_diff.fits' ;percentage difference between throughput

mwrfits,thru_0,out_root+'thru_0.fits'
mwrfits,thru_1,out_root+'thru_1.fits'
mwrfits,thru_3,out_root+'thru_3.fits'
mwrfits,thru_5,out_root+'thru_5.fits'

mwrfits,pol0m1,out_root+'pol0-1.fits'
mwrfits,pol3m5,out_root+'pol3-5.fits'



stop


end