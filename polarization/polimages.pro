pro polimages

root_dir = 'E:\Science\OBS\2013-01-04\GCs\'
file1 = root_dir+'testmNGC1851_V0.300.fits'
file2 = root_dir+'testmNGC1851_V1.301.fits'
file3 = root_dir+'testmNGC1851_V3.302.fits'
file4 = root_dir+'testmNGC1851_V5.303.fits'


a=''
x1 = dblarr(5000)
y1 = dblarr(5000) 
mag1 = dblarr(5000)

x2 = dblarr(5000)
y2 = dblarr(5000) 
mag2 = dblarr(5000)

i=0
close, 1
openr, 1, file1
while ~ EOF(1) do begin
  readf, 1, a
  skip = strpos(a, '#') ; skip the header
  if skip ne -1 then continue
  skip = strpos(a, 'm') 
    if skip ne 0 then begin ; skip ones with file name
      skip = strpos(a, '20.00') ;read the lines with the radius size as the first value
      if skip ne 3 then continue
      line = strsplit(a, " ", /EXTRACT)
      mag1(i-1) = line(4) 
      continue
    endif
  line = strsplit(a, " ", /EXTRACT)
  x1(i) = line(1)
  y1(i) = line(2) 
  i = i+1
endwhile

close, 1
x1 = x1(0:i-1)
y1 = y1(0:i-1)
mag1 = mag1(0:i-1)
a = ''
j=0
close, 2
openr, 2, file2
while ~ EOF(2) do begin
  readf, 2, a
  skip = strpos(a, '#')
  if skip ne -1 then continue
  skip = strpos(a, 'm') ;skip ones that start with the filename
  if skip ne 0 then begin 
     skip = strpos(a, '20.00')
    if skip ne 3 then continue
    
    line = strsplit(a, " ", /EXTRACT)
    
    mag2(j-1) = line(4) 
    continue
  endif
  line = strsplit(a, " ", /EXTRACT)
  x2(j) = line(1)
  y2(j) = line(2) 
  j = j+1
endwhile
close, 2
x2 = x2(0:j-1)
y2 = y2(0:j-1)
mag2 = mag2(0:j-1)

;;;;;;;;;;;;;

a=''
x3 = dblarr(5000)
y3 = dblarr(5000) 
mag3 = dblarr(5000)

x4 = dblarr(5000)
y4 = dblarr(5000) 
mag4 = dblarr(5000)

i=0
close, 1
openr, 1, file3
while ~ EOF(1) do begin
  readf, 1, a
  skip = strpos(a, '#') ; skip the header
  if skip ne -1 then continue
  skip = strpos(a, 'm') 
    if skip ne 0 then begin ; skip ones with file name
      skip = strpos(a, '20.00') ;read the lines with the radius size as the first value
      if skip ne 3 then continue
      line = strsplit(a, " ", /EXTRACT)
      mag3(i-1) = line(4) 
      continue
    endif
  line = strsplit(a, " ", /EXTRACT)
  x3(i) = line(1)
  y3(i) = line(2) 
  i = i+1
endwhile

close, 1
x3 = x3(0:i-1)
y3 = y3(0:i-1)
mag3 = mag3(0:i-1)

j=0
close, 2
openr, 2, file4
while ~ EOF(2) do begin
  readf, 2, a
  skip = strpos(a, '#')
  if skip ne -1 then continue
  skip = strpos(a, 'm') ;skip ones that start with the filename
  if skip ne 0 then begin 
     skip = strpos(a, '20.00')
    if skip ne 3 then continue
    
    line = strsplit(a, " ", /EXTRACT)
    
    mag4(j-1) = line(4) 
    continue
  endif
  line = strsplit(a, " ", /EXTRACT)
  x4(j) = line(1)
  y4(j) = line(2) 
  j = j+1
endwhile
close, 2
x4 = x4(0:j-1)
y4 = y4(0:j-1)
mag4 = mag4(0:j-1)


zeromag = 25. 

im0 = 10^((mag1-zeromag)/2.5)*10^6.
im1 = 10^((mag2-zeromag)/2.5)*10^6.
im3 = 10^((mag3-zeromag)/2.5)*10^6.
im5 = 10^((mag4-zeromag)/2.5)*10^6.

im0_err = sqrt(im0)
im1_err = sqrt(im1)
im3_err = sqrt(im3)
im5_err = sqrt(im5)


stokesparams,im0, im1, im3, im5, im0_err, im1_err, im3_err, im5_err, root_dir+'NGC1851_V_1',x1,y1 



end