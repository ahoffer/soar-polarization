;compute stokes parameters from four images or magnitude lists

pro stokesparams, im0, im1, im3, im5, im0_err, im1_err, im3_err, im5_err, prefix,xpos, ypos

out_file_position = prefix+'.eps'
;calibration factors 0-1 = 1.01, 3-5 = 0.93, 01 - 35 = 1.03
corr01 = 1/0.965
corr35 = 1/0.94
corr0135 = 1.0

I01 = (corr01*im0 + im1)/2.
I35 = (corr35*im3 + im5)/2.
Iavg = (I01+corr0135*I35)/2.
Iavg1 = I01
Q = (corr01*im0-im1)/2.
U = -(corr35*im3-im5)/2. ;NOTE: The sign of U is switched everything looks right
V = sqrt((Q^2.+(corr0135*U)^2.)/Iavg^2.)
max_V = where(V gt 1) ;can't have a fraction gt 1
if max_V(0) ne -1 then V(max_V) = 0
polangle = 0.5*atan(U/Q) 

;error images
I01_err = 0.5*sqrt((corr01*im0_err)^2.+im1_err^2.)
I35_err = 0.5*sqrt((corr35*im3_err)^2.+im5_err^2.)
Iavg_err = 0.5*sqrt(I01_err^2.+(corr0135*I35_err)^2.)

Q_err = I01_err
U_err = I35_err

V_err = sqrt((corr01*Q*Q_err)^2.+(corr35*U*U_err)^2.+(Iavg_err/Iavg)^2.)/(V*Iavg^2.)


; convert the angles such that they are all between 0-180
neg1 = where(Q lt 0 and U gt 0) ; between 45 - 90
polangle(neg1) = polangle(neg1) + 3.14159/2.
neg2 = where(Q lt 0 and U lt 0) ; between 90 - 135
polangle(neg2) = polangle(neg2) + 3.14159/2.
;neg3 = where(Q gt 0 and U lt 0) ; between 135 - 180
;polangle(neg3) = polangle(neg3) + 3.14159

x_mag = V*cos(polangle) 
y_mag = V*sin(polangle)


badvals = where(x_mag ne x_mag)
 if(badvals(0) ne -1) then x_mag(badvals) = 0

badvals = where(y_mag ne y_mag)
if(badvals(0) ne -1) then y_mag(badvals) = 0

;plot the ratios as color coded circles
;bin first

len = size(im0)
len = len(1)
bin_len = round(len/5)
;x_bin = congrid(x2match(nonzero), bin_len)
;y_bin = congrid(y2match(nonzero), bin_len)
;fluxratio_bin = congrid(fluxratio, bin_len)
set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = out_file_position

tvcircle, 100.0, (100.+xpos)*5., (100.+ypos)*5.,round(V*1000000),/DEVICE,/FILL

;create legend
tvcircle, 300.0, 12500.0, 5000.0, round(0.01*1000000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 6000.0, round(0.03*1000000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 7000.0, round(0.05*1000000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 8000.0, round(0.1*1000000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 9000.0, round(0.15*1000000),/DEVICE, /FILL
tvcircle, 300.0, 12500.0, 10000.0, round(0.2*1000000),/DEVICE, /FILL
xyouts,13500.0, 4900.0, '0.01',/DEVICE
xyouts,13500.0, 5900.0, '0.03',/DEVICE
xyouts,13500.0, 6900.0, '0.05',/DEVICE
xyouts,13500.0, 7900.0, '0.1',/DEVICE
xyouts,13500.0, 8900.0, '0.15',/DEVICE
xyouts,13500.0, 9900.0, '0.2',/DEVICE
xyouts,11500.0, 10500.0, 'Pol Percent Legend',/DEVICE
;xyouts,1000.0, 12000.0, file1,/DEVICE
;xyouts,1000.0, 11500.0, file2,/DEVICE

device, /close

set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = prefix+'_hist.eps'
histogauss, V, hist, xrange=[-0.3,0.3]
device, /close


set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = prefix+'_Qhist.eps'
histogauss, Q, hist
plotsym, 1, 3 
oplot,[median(Q)], [hist(0)], psym=8


device, /close
stop


set_plot, 'ps'
showfont, 6, 'Complex Roman', /encapsulated
device, filename = prefix+'_Uhist.eps'
histogauss, U, hist
device, /close


end
