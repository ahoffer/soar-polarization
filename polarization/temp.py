def pol_boxes(reg_list=None, bg_region=None, corr_region=None, corr_fac = None):

	"""Get polarization from a list of regions. Return stokes parameters and fluxes"""
	
	try:
		import scipy.stats as stats
	except ImportError:
		print("Couldn't import scipy.stats.")
	try:
		import pyfits
	except ImportError:
		print("Couldn't import pyfits.")
	try:
		import numpy as np
	except ImportError:
		print("Couldn't import numpy.")
	try:
		import matplotlib.pyplot as plt 
	except ImportError:
		print("Couldn't import matplotlib.pyplot.")
	try:
		import pyregion
	except ImportError:
		print("Couldn't import pyregion.")
	try:
		from astroML.plotting import hist as astrohist
	except ImportError:
		print("Couldn't import astroML.plotting.")

	out_list = np.array([])
	for region in reg_list:	
		# Needs the newline after image to work properly.
		source_region = """image 
		"""+region
		#Make sure that if you use an annulus is isn't in a chip gap.
		bg_region = """image 
		circle(527.53333,1820.9556,90.902427)"""
		#Used to find the correction factor based on the smooth galaxy emission
		corr_region = """image 
		circle(664.88682,81.477289,47.040969)"""
		log_file = 'E:/Documents/aastex52/polarization/polstar_april62013.log'

		RN = 4.4 # read noise electrons per pixel
		GAIN = 2.0 # 2 e-/DN

		#To hand input correction factor to keep the correction factor consistent for a given night
		corr1 = None
		corr3 = 0.944
		corr5 = 1.0
		corr1_err = 0.006287687
		corr3_err = 0.003994353
		corr5_err  = 0.003994353

		"""Load fits images"""
		fits_file_0 = fits_list[0]
		#header = fits_image[0].header # header is a python dictionary
		fits_image_0 = pyfits.open(fits_file_0)
		fits_data_0 = fits_image_0[0].data # Make sure to grab the right FITS extension
		fits_data_0 = fits_data_0.astype('float')

		fits_file_1 = fits_list[1]
		#header = fits_image[0].header # header is a python dictionary
		fits_image_1 = pyfits.open(fits_file_1)
		fits_data_1 = fits_image_1[0].data # Make sure to grab the right FITS extension
		fits_data_1 = fits_data_1.astype('float')

		fits_file_3 = fits_list[2]
		#header = fits_image[0].header # header is a python dictionary
		fits_image_3 = pyfits.open(fits_file_3)
		fits_data_3 = fits_image_3[0].data # Make sure to grab the right FITS extension
		fits_data_3 = fits_data_3.astype('float')

		fits_file_5 = fits_list[3]
		#header = fits_image[0].header # header is a python dictionary
		fits_image_5 = pyfits.open(fits_file_5)
		fits_data_5 = fits_image_5[0].data # Make sure to grab the right FITS extension
		fits_data_5 = fits_data_5.astype('float')

		fits_datat_0 = GAIN*fits_data_0
		fits_datat_1 = GAIN*fits_data_1
		fits_datat_3 = GAIN*fits_data_3
		fits_datat_5 = GAIN*fits_data_5

		"""Create source, background, correction region masks"""

		#x,y are flipped in pyfits arrays so NAXIS2,NAXIS1
		shape = (fits_image_5[0].header["NAXIS2"], fits_image_5[0].header["NAXIS1"])

		region_string = source_region
		r0 = pyregion.parse(region_string)
		m = r0.get_mask(shape=shape)

		bg_region_string = bg_region
		r_bg = pyregion.parse(bg_region_string)
		m_bg = r_bg.get_mask(shape=shape)

		corr_region_string = corr_region
		r_corr = pyregion.parse(corr_region_string)
		m_corr = r_corr.get_mask(shape=shape)

		##test to make sure the region is correct location
		#filt_image = m*fits_image_5[0].data
		#hdu = pyfits.PrimaryHDU(filt_image)
		#hdulist = pyfits.HDUList([hdu])
		#hdulist.writeto('E:/masked_region.fits',clobber=True)

		"""Compute correction factor"""
		if not corr1:
			c0 = fits_data_0*m_corr
			c1 = fits_data_1*m_corr
			c3 = fits_data_3*m_corr
			c5 = fits_data_5*m_corr

			c0 = c0[c0 != 0]
			c1 = c1[c1 != 0]
			c3 = c3[c3 != 0]
			c5 = c5[c5 != 0]

			corr1 = np.sum(c1)/np.sum(c0)
			corr3 = np.sum(c3)/np.sum(c0)
			corr5 = np.sum(c5)/np.sum(c0)

			fits_data_1 = fits_data_1/corr1
			fits_data_3 = fits_data_3/corr3
			fits_data_5 = fits_data_5/corr5

			corr1_err = 1/np.sum(c0)*np.sqrt(np.sum(c1) + np.sum(c1)**2/np.sum(c0))
			corr3_err = 1/np.sum(c0)*np.sqrt(np.sum(c3) + np.sum(c3)**2/np.sum(c0))
			corr5_err = 1/np.sum(c0)*np.sqrt(np.sum(c5) + np.sum(c5)**2/np.sum(c0))
			
		PI = np.pi
		bg_area = np.sum(m_bg)

		bg_0 = m_bg*fits_data_0
		bg_1 = m_bg*fits_data_1
		bg_3 = m_bg*fits_data_3
		bg_5 = m_bg*fits_data_5

		bg_pix_0 = np.sum(bg_0)/bg_area
		bg_pix_1 = np.sum(bg_1)/bg_area
		bg_pix_3 = np.sum(bg_3)/bg_area
		bg_pix_5 = np.sum(bg_5)/bg_area

		fits_data_0 = m*fits_data_0
		fits_data_1 = m*fits_data_1
		fits_data_3 = m*fits_data_3
		fits_data_5 = m*fits_data_5

		# THIS IS A SINGLE VALUE NOT A MATRIX!!
		im0_err = np.sqrt(np.sum(fits_data_0[fits_data_0 != 0]) + RN**2*np.sum(m))
		im1_err = np.sqrt(np.sum(fits_data_1[fits_data_1 != 0]) + RN**2*np.sum(m) + np.sum(m)**2*corr1_err)
		im3_err = np.sqrt(np.sum(fits_data_3[fits_data_3 != 0]) + RN**2*np.sum(m) + np.sum(m)**2*corr3_err)
		im5_err = np.sqrt(np.sum(fits_data_5[fits_data_5 != 0]) + RN**2*np.sum(m) + np.sum(m)**2*corr5_err)
				  
		im0 = fits_data_0[fits_data_0 != 0]-bg_pix_0
		im1 = fits_data_1[fits_data_1 != 0]-bg_pix_1
		im3 = fits_data_3[fits_data_3 != 0]-bg_pix_3
		im5 = fits_data_5[fits_data_5 != 0]-bg_pix_5

		#need to test sometimes it is off by one pixel and it throws and error
		dim = np.shape(im0)
		dim_test = np.shape(im1)

		im0_err = np.sqrt(im0_err**2 + dim[0]**2*(np.std(bg_0[bg_0 != 0])**2+RN**2)/bg_area)
		im1_err = np.sqrt(im1_err**2 + dim[0]**2*(np.std(bg_1[bg_1 != 0])**2+RN**2)/bg_area)
		im3_err = np.sqrt(im3_err**2 + dim[0]**2*(np.std(bg_3[bg_3 != 0])**2+RN**2)/bg_area)
		im5_err = np.sqrt(im5_err**2 + dim[0]**2*(np.std(bg_5[bg_5 != 0])**2+RN**2)/bg_area)
							
		print("Dimension is "+str(dim)+','+str(dim_test))
		print("Bg dimension is "+str(bg_area)+' '+str(np.sum(m_bg)))
		print("Number of pixels is "+str(np.sum(m)))
		print("Should be the same.")

		print("Converted to electrons")

		"""Calculate Stokes Parameters"""
		val0 = np.sum(im0)
		val1 = np.sum(im1)
		val3 = np.sum(im3)
		val5 = np.sum(im5)

		##To look at individual pixels, uncomment. Need to change
		#val0 = im0
		#val1 = im1
		#val3 = im3
		#val5 = im5

		#Convert the standard deviation into a standard error if the pixels are summed
		im0_err = im0_err/np.sqrt(np.sum(m))
		im1_err = im1_err/np.sqrt(np.sum(m))
		im3_err = im3_err/np.sqrt(np.sum(m))
		im5_err = im5_err/np.sqrt(np.sum(m))

		I01 = (val0 + val1)/2.
		I35 = (val3 + val5)/2.
		Iavg = (I01 + I35)/2.
		Q = (val0-val1)/2.
		U = (val3-val5)/2.
		V = ((Q**2.+U**2.)/Iavg**2.)**0.5
		polangle = 0.5*np.arctan(U/Q) 

		I01_err = 0.5*(im0_err**2.+im1_err**2.)**0.5
		I35_err = 0.5*(im3_err**2.+im5_err**2.)**0.5
		Iavg_err = 0.5*(I01_err**2.+I35_err**2.)**0.5

		Q_err = I01_err
		U_err = I35_err
		Vunbias = ((Q**2.+U**2.)**0.5 - (Q_err**2 + U_err**2)**0.5)/np.abs(Iavg)
		V_err = ((Q*Q_err)**2.+(U*U_err)**2.+(Iavg_err*V**2*Iavg)**2.)**0.5/(V*Iavg**2.)

		polangle_err = 0.5/np.abs(Q)/(1+(U/Q)**2)*np.sqrt(Q_err**2*(U/Q)**2+U_err**2)

		# need to change angle range because it is only reported from 0-45
		# conditions based on whether U and Q are + or -
		# convert the angles such that they are all between 0-180

		##WARNING: NEED TO CHANGE TO A LAMBDA OR GENERATOR FUNCTION IF YOU ARE LOOKING AT AN ARRAY


		#between 45 - 90
		if (Q < 0 and U > 0):
			polangle = polangle + PI/2.
		# between 90 - 135
		if (Q < 0 and U < 0):
			polangle = polangle + PI/2.
		#between 135 - 180
		if (Q > 0 and U < 0):
			polangle = polangle + PI

			
		logfile = 'E:/Documents/aastex52/polarization/m87_april.log'
		"""Write to file"""
		f = open(log_file,'a')
		f.write("\n####New Entry\n")
		f.write(str(fits_list)+'\n')
		f.write("Source: "+source_region+'\n')
		f.write("Background: "+bg_region+'\n')
		f.write("Corr1 = "+str(np.round(corr1,4))+", Corr3 = "+str(np.round(corr3,4))+", Corr5 = "+str(np.round(corr5,4)))
		f.write("Counts: 0="+str(np.sum(im0))+", 1="+str(np.sum(im1))+", 3="+str(np.sum(im3))+", 5="+str(np.sum(im5))+'\n')
		f.write("Iavg = "+str(np.round(Iavg,0))+" I01 = "+str(np.round(I01/Iavg,3))+"Iavg I35 = "+str(np.round(I35/Iavg,3))+"Iavg\n")
		f.write("|I01 - I35|/Iavg = "+str(np.round(100*np.abs(I01-I35)/Iavg,3))+"%")
		f.write("Q = "+str(Q)+" +/- "+str(Q_err)+'\n')
		f.write("U = "+str(U)+" +/- "+str(U_err)+'\n')
		f.write("V = "+str(np.round(V*100.,2))+"% +/- "+str(np.round(V_err*100,4))+"%\n")
		f.write("V (unbiased if polarization is low) = "+str(np.round(Vunbias*100,2))+"%\n")
		f.write("Angle = "+str(np.round(polangle*180./PI,2))+" deg\n")
		f.write("V S/N = "+str(np.round(V/V_err,0))+'\n')
		f.close()
		print("Region finished written to: "+logfile)
		
		output = np.array([np.sum(m),val0,im0_err,val1,im1_err,val3,im3_err,val5,im5_err, Iavg, I01, I35, Q, Q_err, U, U_err, V, Vunbias, V_err]
		out_list = np.append(out_list,output)
	
	return out_list 
	
	








