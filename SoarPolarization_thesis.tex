\documentclass[preprint]{aastex}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{epsfig}


\begin{document}
\title{Polarimetry with the SOAR Telescope\footnote{Based on observations obtained at the Southern Astrophysical Research (SOAR) telescope, which is a joint project of the Minist\'{e}rio da Ci\^{e}ncia, Tecnologia, e Inova\c{c}\~{a}o (MCTI) da Rep\'{u}blica Federativa do Brasil, the U.S. National Optical Astronomy Observatory (NOAO), the University of North Carolina at Chapel Hill (UNC), and Michigan State University (MSU).}}

%\author{Aaron S. Hoffer}
%\affil{Michigan State University, Physics \& Astronomy Dept., East Lansing, Mi 48824-2320, hofferaa@msu.edu}

%\author{Megan Donahue}
%\affil{Michigan State University, Physics \& Astronomy Dept., East Lansing, Mi 48824-2320, donahue@pa.msu.edu}

%\and

%\author{William Sparks}
%\affil{STSci Baltimore Md, sparks@stsci.edu}

%\keywords{}

\begin{abstract}
We calibrate the new polarization filters on the Southern Astrophysical Research (SOAR) Telescope on Cerro Pach\'{o}n, Chile. We review the procedure for taking polarization calibration and science images using the SOAR Optical Imager (SOI). They are installed as a set of four linear polarization filters (0$^{\circ}$, 90$^{\circ}$, 45$^{\circ}$, and 135$^{\circ}$) on the SOAR Optical Imager (SOI). As part of the calibration we look at unpolarized targets (globular clusters and unpolarized stars) as well as polarized targets (diffuse nebula and polarized stars). We show that even using a set of four polarization filters instead of the more typical half and quarter wave plate setup on a ground based observatory returns accurate results with a precision of XX\%. In addition to the polarimetry calibrations, we measure the polarization signal from the H$\rm{\alpha}$ filaments of nearby brightest cluster galaxies. The filamentary structure of the H$\alpha$ suggests the gas may be draped by magnetic fields and conduction along these magnetic field lines can cause polarized emission along these filaments.
\end{abstract}

\section{Introduction}

The Hubble Space Telescope has a similar polarization scheme which has a set of four polarization filters, which is typically not used for ground based observatories. Typically, half and quarter wave plate designs are used for ground based optical telescopes because you can determine the polarization vector without taking multiple images to decrease the variability in the sky over observation.

Many mechanisms can cause polarization in the form of emission or the affect from the dust. Polarization studies are typically conducted in IR for the possible dust based polarization while radio observations are typical from synchrotron emission typically from AGN. The optical are also used based on grain polarization or optical synchrotron from large scale magnetic fields in objects such as supernova remnants. The Serkowski law allows for the investigation of wavelength dependence of polarization \citep{1975ApJ...196..261S}. 

In Section 2 we discuss the observations as well as introduce the hardware used in the new polarimetry mode. In Section 3 we present the calibration and data analysis. We take both standard internal calibrations (bias frames and dome flats) as well as on sky calibrations of polarized and unpolarized sources. They help us characterize both the zero level polarization correction between the filters as well as the precision of the polarization fraction and angle. Here we also discuss the variability between nights and how we can obtain reliable polarimetry over multiple nights and flag nights with unreliable photometry. In Section 4 we present our science targets, brightest cluster galaxies (BCGs) with H$\alpha$ filaments. We conclude the paper in Section 5. 

\section{Observations}
The polarization data were taken over five nights with varying weather conditions between November 2010 and December 2011. The dates of observation,  filter selection, exposure times, and sky conditions are listed in Table~\ref{tab:obs}. Each night we took calibration data as well as science targets. For unpolarized standards, we obtained data on XX unpolarized standard stars and XX globular clusters. For polarized standards, we obtained data on XX polarized standard stars and XX diffuse nebula. 

The Southern Astrophysical Research Telescope (SOAR) is a 4.1 m aperture telescope located on Cerro Pach\'{o}n at an altitude of 2,700 meters (8,775 feet) above sea level, at the western edge of the peaks of the Chilean Andes. It was funded by a partnership between the U.S. National Optical Astronomy Observatory (NOAO), the Ministry of Science and Technology of Brazil, Michigan State University (MSU), and the University of North Carolina at Chapel Hill (UNC).

The SOAR Optical Imager (SOI) is composed of two 2k $\times$ 4k detectors. The field of view is $5\arcmin\times5\arcmin$ with a physical chip gap 7.8$\arcsec$ wide between the two detectors. Typically, objects are dithered at least twice to have three locations for the target in a given night. The objects are typically offset from center in all dithers to prevent the majority of the emission from falling into the chip gap. The notable exceptions are the globular clusters which cover the whole field of view and the stars which fall in the chip gap are not of greater importance than the surrounding stars in the cluster. However these receive small dithers as well. In some cases the field of view is restricted as some of the older narrow band filters are $2"\times2"$ instead of the full field $4"\times4"$ filters. Those smaller sized filters are noted in Table~\ref{tab:obs}. We use the standard image binning of  2$\times$2 because the binned pixel size (0.15$\arcsec$/pixel) is much smaller than the seeing. The filters can be placed into two filter wheels which can hold up to five filters in each wheel. The four polarization filters are placed in the filter wheel closest to the sky. This leaves one open filter position in that wheel to preserve the ability to take images without the polarization filters in. The second wheel can then be used for any additional narrow or broadband filters. Typically you can fill the second wheel with five filters, but during some nights we left one open to take images with just polarization filters to test the throughput of the filters. We use the broadband B, V, R, and I filters as well as additional filters from the CTIO narrow band filter list\footnote{\url{http://www.ctio.noao.edu/instruments/filters/}}.

The polarization filters were purchased by STScI from the grant XXXX and are the same as the filters used for the polarization mode on the Hubble Space Telescope. There are four linear polarizing filters ($0^{\circ}, 45^{\circ}, 90^{\circ}, 135^{\circ}$). The 0$^{\circ}$ and 90$^{\circ}$ filters are cut from the same piece of glass as are the 45$^{\circ}$ and 135$^{\circ}$ filters. Since there are two filters (i.e. a polarizer and a narrow- or broadband filter) in the lightpath instead of one, the focus is different than the focus with a single filter. Prior to our November 23$^{\rm{rd}}$ night we did not determine a separate focus which may have caused unfocused images prior to that night. The PSF of a sampling of field stars gives results that are XX$\times$ worse for previous nights.

\section{Calibration and Data Analysis}
Prior to taking observations of scientific targets, we take calibrations from a variety of sources. In addition to dome flats, we take on sky calibrations including: unpolarized standard stars, globular clusters, polarized standard stars, and diffuse nebula. While the unpolarized and polarized standard stars are more precisely calibrated, the extended sources (i.e. globular clusters and diffuse nebula) provide more useful to estimate the proper calibration of the whole filter. The individual polarization standard stars can only be reliable for a small section of the filter unless they are dithered over a significant fraction of the CCD. However, once the relative calibration across the filter is known, the individual polarization standard stars can also be used to verify the absolute calibration. HST has a list of calibration sources \citep{1992AJ....104.1563S,1990AJ.....99.1243T}. UKIRT also has a list of standards that can be used on their website\footnote{\url{http://www.jach.hawaii.edu/UKIRT/instruments/irpol/irpol\_stds.html}}. The UKIRT site incldues targets from \citet{1992ApJ...386..562W,1974psns.coll.....G,1974psns.coll..135S}. However, most of these are northern hemisphere objects. 

\subsection{Dome Flats}
For a dome flat, light is projected onto a white screen such that the light is scattered and unpolarized. Twilight flats, on the other hand, can be polarized and therefore will be unreliable for use as a calibration source. Additionally, the dome flats can correct for any polarization of the light path itself. The dome flats were taken for all combinations of astronomical filters and polarization filters used. We reduce the images by flat fielding with all matching combinations. We compare this result to only using one polarization filter (0$^{\circ}$ filter) to calibrate all the filters by comparing the count differences between each of the four polarizers (i.e. is it necessary to take a flat with all four polarization filters for a given science filter or is the throughput similar enough and the lightpath sufficiently unpolarized to use only one of the polarization filters with each image). To examine throughput of the polarizers, we take a dome flat with a broadband filter and compare it to a configuration with the polarizer in with that same broadband filter. Each polarizer should cut the intensity of light in half after normalizing for the broadband filter. The difference from the theoretical throughput is XX\% for the 0$^{\circ}$ and 90$^{\circ}$ and XX\% for the 45$^{\circ}$ and 135$^{\circ}$ filters with the largest deviation of XX\%  in XX filters. We also compare the sum of the intensity of the 0$^{\circ}$ and 90$^{\circ}$ images to that of the 45$^{\circ}$ and 135$^{\circ}$ images. Each sum should return the original intensity while the difference in the images should return no signal. Given that each pair is cut from the same piece of glass the throughput should be the same, therefore, this may indicate that the light path is polarized or that the filters are not quite perpendicular to each other and there may be a correction that must be made. We compare the normalized V + polarizer to the normalized R + polarizer to get a V-R color for the polarizers. The color distribution in absolute intensity is plotted as a histogram for each of the polarizers in Figure XX. The mean color is XX$\pm$XX mag.


\subsection{Unpolarized Targets}

We obtain polarization images of unpolarized standard stars as well as globular clusters to estimate the relative throughput of each pair of polarization filters. We observed each of the targets with the V and R filters as another test of the color dependence of the polarization filters. The corrections needed for calibration are presented in Table XX. As another method of correction in comparison with the dome flats, we use aperture photometry on the unpolarized standard star to estimate the correction to the calculation of the Stokes parameters. As before the $0^{\circ}-90^{\circ}$ degree filters and the $45^{\circ}-135^{\circ}$  should have the same throughput however, there needs to be correction between the pairs as well. 

We also look at globular clusters as an example of an unpolarized extended source. Even though globular clusters are mostly unpolarized, some of the stars or regions of the globular cluster may have a small amount of polarization both intrinsic to the globular cluster as well as part of the line of sight ISM signal. Therefore, in our investigation of the variability of the polarization signal as a function of position on the filter, we use stars extracted from multiple globular clusters. Stars are found in each of the images using the IRAF DAOPHOT procedure for detections $>$ 10 $\rm{\sigma}$ which are not saturated. This also gives a list of stellar magnitudes which can be used to compute the relative throughput of the polarization filter pairs. We plot the distribution of the $45^{\circ}-135^{\circ}$ relative fluxes in Figure~\ref{fig:histogram}. The stars from the $0^{\circ}-90^{\circ}$ images are centered on a normalized flux of 1.  The median of each distribution is used to compute the offsets used in the Stokes parameters. This gives a relative correction of $XX\% \pm XX\%$ between each of the sets of filters. 

We also examined some stars in the field of our targets as a form of unpolarized source calibration. Assuming these individual stars have a small fractional polarization, we can use them to estimate the polarization of the sky in that region. A large polarization fraction ($>$ 5\%) for a star would suggest that there are clouds in the field making the polarization signal unrealiable. If this is not the case, we can use the stars in the field as an additional tool to make an on sky correction to the data. 

\subsection{Polarized Targets}
We analyze a polarized standard star with the same  procedure as the one used for the unpolarized standard star. The calibration of the polarized standard gives a measurement of the precision of the polarization fraction as well as the accuracy of the angle of the polarization vector. However, the term polarized standard star can be very misleading. Typically these stars have a polarization of a few percent and a variety of polarization angles, yet these sources can be quite variable. These stars are shrouded by dust which can change over short timescales and create a variable signal making both the measurement of polarization fraction as well as angle difficult to obtain consistent measurements. We use difffuse nebula emission as an additional calibration to ensure the polarization filters were oriented correctly in the filter wheel and measurer the offset in any angle from the positioning of the filter in the filter holder.   

The Crab Nebula (M1) is a pulsar wind nebula with a very high polarization ($\sim$25\%). The most recent optical polarization images of the Crab Nebula are from \citep{1990ApJ...365..224H,1979ApJ...227..106S,1974MNRAS.166..617W,1957BAN....13..301W}. Many more optical polarization measurements have been made of the pulsar itself. The polarization field lines are overplotted on the V band image of the nebula in Figure~\ref{fig:crab}. R Monocerotis is a T Tauri star which is part of the diffuse nebula NGC 2261. Given the youth of the star (XX Myrs) the polarization vectors can change on very short timescales (XX years?) due to the variability of the source. While many of the polarization standard stars likely have circumstellar dust which cause their polarized light, the star in the nebula R Mon is a good test because the polarization vectors form a ring around the central star.  

\subsection{Data Analysis}
The images are initially reduced through the SOAR pipeline written by Nathan DeLee \footnote{\url{http://khan.pa.msu.edu/www/SOI/}}. The pipeline is a collection of IRAF tools that will bias correct and flat field the images as well as stitch the mosaic FITS file of four CCD segments into one FITS file with the physical chip gap. WCS corrections are done separately by editing the image header WCS terms using a 2MASS source (typically a star) in the field. We dither at least three positions on the sky for each object. Individual exposures are aligned, trimmed, and stacked to remove cosmic rays as well as the chip gap. Once the stacked exposures are reduced, we compute Stokes parameters to determine the linear polarization. We also analyze individual exposures before stacking to ensure there was not an effect on the polarization measurment becuase of varying weather conditions. We do not compute a circular polarization term. We create two difference images to determine the Stokes parameters for $0^{\circ}-90^{\circ}$ (Q) and $45^{\circ}-135^{\circ}$(U). The total intensity, I,  is the average of all four polarized images. The polarization intensity of the image is $I_P = (Q^2+U^2)^{1/2}$ and the polarization angle is calculated by $\theta_P =1/2 \rm{arctan}(U/Q)$. The polarization fraction is $P = I_P/I$. Multiple dithered images are taken and the dithered images are stacked before the Stokes parameters are calculated. The Stokes parameter maps are constructed with an IDL script which uses the set of four reduced polarization images. When binning the data we use the CONGRID IDL routine and do not interpolate across cells. As well as images for each of the Stokes parameters I, Q, and U, we create maps for the fractional polarization and signal to noise ratio of the fractional polarization. SAOimage DS9 vector regions are also created for the polarization vectors as well. For the creation of polarization fraction vectors, a minimum S/N ratio of 5 is used unless otherwise indicated. Images are binned to improve the signal to noise level of detections if the data are too noisy.

\subsection{Upper limit estimates}
Initial estimates of the polarization maps of H$\alpha$ don't seem to show any high S/N polarization and we will likely need to estimate upper limits. I still need to come up with a procedure. Possibly related to how well we can measure the polarization of the standard targets. I will start working on this once I have a better handle on the calibrations after the run this week. We may also want to summarize the precision with which we can measure the angle of polarization as well as a systematic error for lowest level of polarization we can measure.

\section{Science Targets}
We investigate the polarization signature in the H$\alpha$ filaments of brightest cluster galaxies. BCGs are the brightest galaxies in galaxy clusters and are usually centrally dominant (cD) galaxies located near the bottom of the potential wells in galaxy clusters. Many BCGs found in low entropy galaxy clusters have a significant detections in H$\alpha$ and some of those nearby BCGs show interesting filamentary structure in their H$\alpha$+[NII] narrow band images.  In addition to binning the data, we use the contour binning technique from \citet{2006MNRAS.371..829S} to create regions of individual H$\alpha$ filaments. This method bins along surface brightness features and preferentially creates binned regions which are long filaments. This is typically mitigated by constraining the ratio of the length to the width of the binned region. However, since our features are actually long filamentary structures, we do not apply this constraint. If there is magnetic field draping, the polarization vector should be coherent along the filament and the net polarization of the filament should not be suppressed as if there were randomized polarization vectors. Instead the only constraint we set is on the minimum S/N of XX. For contour binning, the H$\alpha$ filament regions are determined using the 0$^{\circ}$ image and that region is applied to all four filters. If each of the polarization angles was individually extracted for a region, the regions may be sytematically different for reasons related to the method of extraction used by the code and not just a difference in the polarization of the filament. Also, when the contour binning technique is used the images are not stacked to prevent variability of the PSF degrading our sharper images. However, maps of stacked and unstacked images are made for the other polarization maps.

\subsection{M87 AGN Polarization}
M87 (NGC 4486) is the brightest cluster galaxy in the nearby Virgo Cluster. It contains an AGN which is known to have a polarized radio jet which has also been observed optically with HST \citep{1999AJ....117.2185P}. To measure the  polarization of the jet we observe the polarization in the V-band. M87 is an elliptical galaxy such that most of the V band light is unpolarized stars. Prior to computing the Stokes parameters, the unpolarized elliptical galaxy emission was removed used with {\it ellipse} in IRAF. We reproduce Figure~\ref{fig:AGN} from \citet{1999AJ....117.2185P} and overlay our polarization vectors. The extracted region in our SOAR data is coincident with the AGN outflow 11$\arcsec$-18$\arcsec$ from the center of the galaxy seen in the HST image.  

\subsection{M87 H$\alpha$ Polarization}
We also calculate the net H$\alpha$ image of M87. We use the filter 6600-75 from the CTIO H$\alpha$+[NII] narrow band filter set. We also take a narrow band continuum filter 6129-140 to create the net image. We bin the data 4$\times$4 to improve the signal to noise while keeping the filamentary structure intact. Additional binning is best achieved with the contour binning technique described above. 


\subsection{NGC4696 H$\alpha$ Polarization}
Similarly we create a net H$\alpha$+[NII] image for NGC 4696 to measure polarization.

\subsection{Other BCG H$\alpha$ Polarization: Hydra A, Abell 133, Abell 496 }
We present additional H$\alpha$ images in Figure XX. We measure total polarization in the H$\alpha$ image for each of the BCGs.

\section{Conclusions}

The SOAR telescope can now be used to measure optical polarization with the SOAR Optical Imager. We find that we can detect levels of polarization down to XX\% with a S/N $>$ XX. We can also measure the polarization angle of the sources to an uncertainty in the angle of $\pm$XX$^{\circ}$. We look for possible sources of polarization in the H$\alpha$ filaments of nearby BCGs and find an upper limit on the level of conduction given the upper limit on the polarization of XX percent.

\section{Polarization}
Explain polarzation from an EM point/fundamentalphysics

\subsection{Astrophysical Polarization}

Final point: you will definitely want to be very clear about the science in the polarimetry chapter. Each sentence in this
outline can be expanded into a paragraph OR MORE  (most will require more than a single paragraph to do justice. Also
feel free to use pictures, graphs, tables, diagrams to make your explanation more quantitative and clear)
1. Explain what causes polarization from an EM / Jackson style perspective - fundamental physics.
2. Explain what in astrophysics can cause light to be polarized.
3. Explain why we are testing for polarization in the  M87 filaments (you can use Bill Spark's proposals and his draft of the
spectropolarimetry paper which I believe I sent to you for the concepts and the relevant references -- use your own words
to describe it BUT you do not have to invent this part.

\section{SOI Observations and Reduction}
Table of observation dates, types of observations taken, modes used for SOI, about the instrument, explanation of Nathan's scripts.


\section{Calibrations}

I have text on this so I won't repeat it now until I add that text here


4. Explain how and when the observations were done and what calibrations you did (targets: standard stars, clusters of stars, polarized
stars, polarized Crab Nebula and M87 jet, science study of the off-nuclear M87 emission line filaments).

5. Present the results of the calibration of the system on Crab and M87 jet, compare to past results. Worked on high percent. (we know this worked, good)
5. Explain the calibration tests and quantify the statistical accuracy of the clusters photometry tests on the filters. (polarization filter pairs are
        statistically identical - right?, I haven't heard whether the full set is stat id -- but each PAIR were cut from identical glass so they
        better be; the question is whether all 4 may be cut from identical glass.) Compare the ratio you get that is different to 1.000 to the
        statistica l error. If the ratio is different from 1.000 at a level larger than the statistical uncertainty, then there is a difference to
        explain. Observations taken during non-photometric conditions can be different, but then you'd get different ratios on different
        nights or different times, so that's easy to check. The other difference can be from flat fielding ( what affects flat fielding : dome
        illumination differences, scattered light - which is impossible to flat field ). IF THERE ARE DIFFERENCES between observations
        taken with the same filter and they are statistically significant you must explain them.
\subsection{Zero Polarization Integrity}
Use globular clusters to show that there is no difference between the filter sets. What limit can we get down to?


\subsection{High Polarization Integrity}
Show results from the crab nebula, flame nebula, and M87 jet to demonstrate that we can accurately and precisely reproduce polarization.


\subsection{BCG Analysis}



You can take these steps separately for Halpha and Continuum (V or what ever you used)

For every pair of *IDENTICAL* sky locations and aperture definitions (taken of the same part of sky)
you can build a histogram of all the pixel differences between images -- both individual and combineded -- over a given set of areas.
The individual images would give larger errors on the mean but I'll describe a test below where the individual images
are interesting.

The areas would
need to be big enough to get a statistically interesting histogram, but native SOI pixels are pretty small, 0.077" so even if you're
binning the image 2x2 the pixels are small.

So you could start with small rectangular apertures (make sure the IDs are the same for every image): 2" x 2" would still have
675 pixels! They could be elongated along the filaments, but still have a lot of pixels in them.

Then compare the histograms (you can do the mean, standard deviation, and a KS test) between histograms made
from different images of the same place on the sky. You would have
   a. A set of comparisons made through IDENTICAL images
   b. A set of comparisons made through IDENTICAL  but 90-Degree different polarizer filters.

If the distribution in "a" is statistically identical to the distribution of differences in "b" you have the expected null result
and you can quantify the difference (if the distributions are very nearly gaussian) by the limits on the errors in the means.
The KS test doesn't care if the distribution itself is Gaussian, so that test can compare two samples of points and ask
what is the probability they were drawn randomly from the same parent population.  The KS test should give very high
probabilities ( > 0.03) for KS test run between pairs of sky samples in a no matter what, and for b -- if there is no polarization --
it should give the same result.

The other classic test is the "student's T test" -- I believe this applies in this case because the S/N in the identical images should
be .. identical. You are taking light from the same optical filter (e.g. the halpha filter) in both cases, exposing for the same amount of
time). You can look this up on Wikipedia, there are even on-line tools you could use to check your own calculation.

You can also separate the "identical" tests into tests taken at nearly exactly the same dither position and tests made between
the identical observation but two different dither positions. Those tests will quantify how good your flat fielding was, averaged
out over the scales you're looking.   We're not too worried about individual pixels flattening slightly differently but of larger areas
varying depending on where they sit on the detector. That is mitigated because we DO dither, but it's a good quantity to know.




\subsection{Limits on BCG Polarization}


These tests are limited by the fact that the PSF changes will affect some apertures differently from others -- so you wouldn't be able
to compare the images of the terrible seeing night(s) we had to the wonderful seeing nights. But I think you have enough high quality
data to make these basic tests.

6.  Explain we have a null result for M87 filaments, and now the work is to explain our limits. That is the work of these histogram
comparisons I described above, compared by : mean, standard deviation, KS test and student T-test.
        The means might differ if there are photometry differences but that will occur over the entire image uniformly.
        The standard deviations or widths might differ if the S/N changes from one image to another. That should NOT have happened.
        There will be small variations due to seeing variations for apertures which are smallish. But once you get to the larger apertures
        that will be minimized.
        You should do at least one test where you take a mask of the whole off-axis filament (that would be your "biggest" aperture) and
        make the same comparison. If there were regions of detected polarization in the somewhat smaller apertures, the biggest
        aperture comparison will scramble them, because the different polarization directions will cancel each other out. But if there
        was NO systematic correlation of polarization in the somewhat smaller apertures, the comparison of the largest aperture will
        be the best test comparing IDENTICAL images -- the flat fielding differences are washed out, and the histogram of all the
        pixels in that big aperture (should be 10s of thousands of pixels in the mega-aperture) should have the SAME standard deviation,
        a negligible KS test.



\acknowledgments
Support for this research was provided by a JPL Spitzer Archival Contract (GET NUMBER) and a Spitzer DDT award (GET NUMBER). STScI Grant (GET NUMBER) for purchase of polarization filters. Thank SOAR operators Daniel Maturana, Patricio Ugarte, Sergio Pizarro, Alberto Pasten for their help during observing nights. 

%\appendix

%\section{Observing Run Notes}

%Here we go through the process of setting up a polarimetry run on SOAR. 

\begin{figure}
\centering
\begin{tabular}{cc}
\epsfig{file=./hist-01-1.eps,width=0.5\linewidth,clip=} & 
\epsfig{file=./hist-35-1.eps,width=0.5\linewidth,clip=} \\
\epsfig{file=./hist-01-2.eps,width=0.5\linewidth,clip=} &
\epsfig{file=./hist-35-2.eps,width=0.5\linewidth,clip=}\\
\epsfig{file=./hist-01-3.eps,width=0.5\linewidth,clip=} & 
\epsfig{file=./hist-35-3.eps,width=0.5\linewidth,clip=} \\
\epsfig{file=./hist-01-4.eps,width=0.5\linewidth,clip=} &
\epsfig{file=./hist-35-4.eps,width=0.5\linewidth,clip=}\\
\end{tabular}
\caption{Histograms of NGC1851 stellar fluxes. We present all of the globular cluster histograms for NGC1851. The four plots on the first column are 0-90 relative comparisons. The set on the right are 45$^{\circ}$-135$^{\circ}$ comparisons.}
\label{fig:histogram}
\end{figure}

\begin{figure}
\centering
\begin{tabular}{cc}
\epsfig{file=./M87agn.eps,width=0.5\linewidth,clip=} & 
\epsfig{file=./AGN.eps,width=0.5\linewidth,clip=} \\
\end{tabular}
\caption{M87 AGN polarization map.The plot on the left is the the polarization map from  \citet{1999AJ....117.2185P}.  }
\label{fig:AGN}
\end{figure}

\begin{figure}
\includegraphics[scale = 0.3]{./crab.eps}
\caption{Polarization map of the Crab Nebula. V band image of the Crab Nebula with polarization vectors overplotted. There is a 10\%  }
\label{fig:crab}
\end{figure}

%\begin{figure}
%\includegraphics[scale = 0.8]{./halphaBCGmaps.eps}
%\caption{Polarization map of the Crab Nebula. V band image of the Crab Nebula with polarization vectors overplotted. There is a 10\%  }
%\label{fig:halphabcgs}
%\end{figure}



\bibliographystyle{apj}	% (uses file "apj.bst")
\bibliography{SoarPolarization}		% expects file "SoarPolarization.bib"


\begin{deluxetable}{cccccc}

%% landscape mode
\rotate
\label{tab:obs}
%% Over-ride the default font size
%% Use Default (12pt)

%% Use \tablewidth{?pt} to over-ride the default table width.
%% If you are unhappy with the default look at the end of the
%% *.log file to see what the default was set at before adjusting
%% this value.

%% This is the title of the table.
\tablecaption{Observations}

%% The \tablehead gives provides the column headers.  It
%% is currently set up so that the column labels are on the
%% top line and the units surrounded by ()s are in the 
%% bottom line.  You may add more header information by writing
%% another line between these lines. For each column that requries
%% extra information be sure to include a \colhead{text} command
%% and remember to end any extra lines with \\ and include the 
%% correct number of &s.
\tablehead{\colhead{Target} & \colhead{$\alpha$(2000)} & \colhead{$\delta$(2000)} & \colhead{Filters} & \colhead{Exposure Time} & \colhead{Nights Observed} \\ 
\colhead{(---)} & \colhead{(---)} & \colhead{(---)} & \colhead{(---)} & \colhead{(seconds)} & \colhead{(---)} } 

%% All data must appear between the \startdata and \enddata commands
\startdata
\cutinhead{Unpolarized Standards}
HD94851 & 10$^h$56$^m$44.26$^s$  & -20$^{\circ}$39$^m$53$^s$ & V & 1000 & a,b,c,d,e \\
NGC 1851 & 05$^h$14$^m$06.7$^s$ &  -40$^{\circ}$02$^m$48$^s$ & V & 1000 & a,b,c,d,e \\
47 Tuc & 00$^h$24$^m$05.3$^s$ &  -72$^{\circ}$04$^m$53$^s$ & V & 1000 & a,b,c,d,e \\
\cutinhead{Polarized Standards}
HD110984 & 12$^h$46$^m$44.83$^s$  & -61$^{\circ}$11$^m$11.58$^s$ & V & 1000 & a,b,c,d,e \\
HD298383 & 09$^h$22$^m$29.77$^s$ &  -52$^{\circ}$28$^m$57.27$^s$ & V & 1000 & a,b,c,d,e \\
Crab Nebula & 05$^h$34$^m$32.0$^s$ &  +22$^{\circ}$00$^m$52$^s$ & V & 1000 & a,b,c,d,e \\
Flame Nebula & 05$^h$41$^m$42.7$^s$  & -01$^{\circ}$54$^m$44$^s$ & V & 1000 & a,b,c,d,e \\
R Monocerotis & 06$^h$39$^m$09.95$^s$  & +08$^{\circ}$44$^m$09.7$^s$ & V & 1000 & a,b,c,d,e \\
\cutinhead{BCG Science Targets}
M87 & 12$^h$30$^m$49.4$^s$ &  +12$^{\circ}$23$^m$28$^s$ & V,6600-75\tablenotemark{a},6129-140\tablenotemark{b} & 1000 & a,b,c,d,e \\
NGC 4696 & 12$^h$48$^m$49.2$^s$  & -41$^{\circ}$18$^m$39$^s$ & 6600-75\tablenotemark{a},6129-140\tablenotemark{b} & 1000 & a,b,c,d,e \\
Hydra A & 09$^h$18$^m$05.7$^s$ &  -12$^{\circ}$05$^m$44$^s$ & V, R, 6916-78\tablenotemark{a}\tablenotemark{c},6600-75\tablenotemark{b} & 1000 & a,b,c,d,e \\
Abell 133 & 01$^h$02$^m$39.0$^s$ &  -21$^{\circ}$57$^m$15$^s$ & V,R,6916-78\tablenotemark{a}\tablenotemark{c},6600-75\tablenotemark{b} & 1000 & a,b,c,d,e \\
Abell 496 & 04$^h$33$^m$37.1$^s$  & -13$^{\circ}$14$^m$46$^s$ & 6781-78\tablenotemark{a},6120-140\tablenotemark{b} & 1000 & a,b,c,d,e \\
\enddata

%% Include any \tablenotetext{key}{text}, \tablerefs{ref list},
%% or \tablecomments{text} between the \enddata and 
%% \end{deluxetable} commands

%% General table comment marker
\tablecomments{The total exposure times calculated are from all nights for each filter. Observations are taken:
(a) December 4$^{\rm{th}}$, 2010; (b) January 7$^{\rm{th}}$, 2011; (c) April 7$^{\rm{th}}$, 2011; (d) November 23$^{\rm{rd}}$; (e) December 27$^{\rm{th}}$, 2011}
\tablenotetext{a}{This filter was used for a narrow band H$\alpha$ filter for this target.}
\tablenotetext{b}{This filter was used for a narrow band continuum filter for this target.}
\tablenotetext{c}{Filter is the \tablenotemark{c} $2"\times2"$ field.}
%% No \tablerefs indicated

\end{deluxetable}




\end{document}



