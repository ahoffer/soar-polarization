% Michigan State University Thesis Class
% Copyright (c) 2006 Christopher Waters <watersc1@pa.msu.edu>
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.

% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
%
% $Id: msuthesis.cls,v 1.3 2008/07/09 20:20:06 delee Exp $
% $Date: 2008/07/09 20:20:06 $
% $Revision: 1.3 $
%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{msuthesis}
% This is built off of the book class, with the other packages adding
% capabilities that are needed.
\LoadClass[12pt]{book}
\RequirePackage{setspace}       % This holds the doublespacing options
\RequirePackage{natbib}         % This allows the citet/citep commands
\RequirePackage{deluxetable}    % Because it's better than regular table
\RequirePackage{longtable}      % Because deluxetable sucks at captions
\RequirePackage{lscape}         % For landscaped tables.
\RequirePackage{amssymb}        % I really didn't think anyone needed these.
\RequirePackage{amsmath}
\RequirePackage[final]{graphicx}% And you can quit your damn bitching, Brian.
\RequirePackage{psfrag}
% Define a draft option.  Doublespacing is teh suck
\DeclareOption{draft}{%
  \setlength{\overfullrule}{5pt}
  \gdef\@draftstyle{\relax}
  \PackageWarning{msuthesis}{Warning!  This option `\CurrentOption' breaks the formatting required by the Graduate School.}
}
\DeclareOption{noorphans}{%
  % I hate widows and orphans, and TeX doesn't enough.
  \clubpenalty=9999
  \widowpenalty=9999
}
\DeclareOption{final}{%
  % I hate you graduate school.  I hope you die.
  \gdef\@finalstyle{\relax}
  \PackageWarning{msuthesis}{Warning!  This option `\CurrentOption' may comply with the formatting required by the Graduate School, but may also make it look dumb!}
}
\DeclareOption*{%
  \PackageWarning{msuthesis}{Unknown Option `\CurrentOption'}%
}
\ProcessOptions\relax


% Define the papersize to be letter.
\setlength\paperheight{11in} 
\setlength\paperwidth{8.5in}
% Set margins
\setlength{\topmargin}{0.0in}
\setlength{\headheight}{0.0in}
\setlength{\headsep}{0.0in}
\setlength{\oddsidemargin}{0.5in}
\setlength{\evensidemargin}{0.5in}
\setlength{\textwidth}{6.0in}
\setlength{\textheight}{9.0in}
\setlength{\LTcapwidth}{\textwidth}
% Set font sizes here.  This should keep all the fonts large enough
% so the grad school doesn't get bitchy.
\newcommand{\bpfont}{\fontfamily{phv}\fontsize{11}{11}\selectfont}
\newcommand{\bpfonttiny}{\fontfamily{phv}\fontshape{it}\fontsize{8}{8}\selectfont}
\renewcommand{\tiny}{\@setfontsize\tiny\@viiipt{9.5}}
\DeclareMathSizes{12}{12}{10}{8}

% Just to be sure, let's hard code the font sizes (as much as it makes me sad).
% I've listed the original fontsizes for everything in comments
\renewcommand{\tiny}{\fontsize{8}{9.6}\selectfont}            %  5 /  5
\renewcommand{\scriptsize}{\fontsize{8}{9.6}\selectfont}      %  7 /  7
\renewcommand{\footnotesize}{\fontsize{12}{12}\selectfont}    %  8 /  8
\renewcommand{\small}{\fontsize{11}{13.2}\selectfont}         %  9 /  9
\renewcommand{\normalsize}{\fontsize{12}{14.4}\selectfont}    % 10 / 10
\renewcommand{\large}{\fontsize{14}{16.8}\selectfont}         % 12 / 12
\renewcommand{\Large}{\fontsize{16}{19.2}\selectfont}         % 14 / 14.4
% Larger fonts I've left alone.  What are you doing using that big of a font
% anyway?

% These commands set variables that are used to ensure that you don't need
% to type in the information more than once.  All of the subsequent formatting
% commands require these be set before hand.

% These two should be self-explanatory.
\renewcommand*{\title}[1]{\gdef\@title{#1}}
\renewcommand*{\author}[1]{\gdef\@author{#1}}
% The /degree/ is the actual title of the degree.  This should probably be
% /Doctor of Philosophy/.
\newcommand*{\degree}[1]{\gdef\@degree{#1}}
% The /subject/ is what your degree will be in.  Mine is /Astrophysics/.  The
% /department/ is the full department name.  Mine is 
% /Department of Physics and Astronomy/.
\newcommand*{\subject}[1]{\gdef\@subject{#1}}
\newcommand*{\department}[1]{\gdef\@department{#1}}
\newcommand*{\advisor}[1]{\gdef\@advisorname{#1}}
% You should get the copyright to your thesis.  Fill this in with the year.
\newcommand{\copyrightyear}[1]{\gdef\@copyrightyear{#1}}
% This is where you fill in all of the long text entries.  The /dedication/,
% /acknowledgments/ , and /preface/ are all optional.  If you do not define
% them, those pages will not show up in the final copy.
\newcommand{\abstract}[1]{\long\def\@abstract{#1}}
\newcommand{\dedication}[1]{\long\gdef\@dedication{#1}}
\newcommand{\acknowledgments}[1]{\long\gdef\@acknowledgments{#1}}
\newcommand{\preface}[1]{\long\gdef\@preface{#1}}
% This class cannot calculate how many volumes your thesis takes up, nor can
% it automatically break it up.  If you decide to break it up, set this in the
% preamble of the document.  The value doesn't matter (it does /not/ hold the
% current volume).  A simple '\multivolume{}' is good enough.
% To insert a new volume, you need to call another command later.
\newcommand{\multivolume}[1][1]{\gdef\@multivolume{#1}}
\newcommand{\colorfigures}{\gdef\@colorfigures{true}}

% Set up all the new counters that I use.
% /volume/ holds the current volume.  Even if you don't need it, it's here.
\newcounter{volume}

% This section defines all of the new sections that are required by the 
% Graduate School.  None of these should ever need to be directly called.
% The /requirements/ command calls all of these, ensuring they show up in
% the correct order.

% This is the bookplate, that /may not be altered/, and /must be the on-line
% interactive bookplate/.  Whatever.  It's dumb, and I've faked it up here.  
% It's not 100% perfect, but I'll probably get around to fixing it at some 
% point.  This version will also always center all lines together, unlike 
% the authentic bookplate.  That makes mine not retarded.
\newcommand*{\bookplate}{%
  \newpage
  \singlespacing
  \begin{center}
    \begin{minipage}[c][\textheight]{5.5in}
      \begin{center}
        {\bpfont  This is to certify that the}\\
        {\bpfont dissertation entitled}\\
	{\bpfont \strut}\\
        \rule[-68pt]{0pt}{136pt}\begin{minipage}[c]{4.5in}
          \begin{center}
            {\bpfont \@title}
          \end{center}
        \end{minipage}\\
	{\bpfont \strut}\\
        {\bpfont presented by}\\
	{\bpfont \strut}\\
        \rule[-47pt]{0pt}{94pt}\begin{minipage}[c]{4.5in}
          \begin{center}
            {\bpfont \@author}
          \end{center}
        \end{minipage}\\
	{\bpfont \strut}\\
        {\bpfont has been accepted towards fulfillment}\\
        {\bpfont of the requirements for the}\\
	\strut\\
        \underline{\strut\makebox[1in]{\bpfont \centering Ph.D.}}
        \makebox[1.0625in]{\bpfont \centering degree in}
        \underline{\strut\makebox[2.3125in]{\bpfont \centering \@subject}}\\
	\strut \\
	\strut \\
	\strut \\
        {\bpfont \underline{\strut\makebox[3.78125in]{}}} \\
	{\bpfont \strut Major Professor's Signature}\\
	{\bpfont \strut}\\
        {\bpfont \underline{\strut\makebox[2.28125in]{}}} \\
	{\bpfont \strut}\\
        {\bpfont \strut Date}\\
	{\bpfont \strut}\\
	\vspace*{0.30in}
        {\bpfonttiny MSU is an affirmative action, equal-opportunity employer}
      \end{center}
    \end{minipage}
    \end{center}
  \newpage
}
%% \newcommand*{\bookplate}{%
%%   \singlespacing
%%   \psfrag{REPLACE THIS TEXT WITH YOUR NAME }{\@author}
%%   \psfrag{REPLACE THIS TEXT WITH YOUR DISSERTATION TITLE }{\@title}
%%   \psfrag{REPSUBJ}{\@subject}
%%   \vspace*{1.5in}\hspace*{-2.0in}\includegraphics[clip=false]{/tmp/fuckyou.ps}
%%   \newpage
%% }
% /titlepage/ already exists, so we redefine it here to match the requirements.
% I may alter fonts at some later date.
\renewcommand*{\titlepage}{%
  \newpage
  \singlespacing
  \thispagestyle{empty}
  \begin{center}
   \vspace*{1in}
    \MakeUppercase{\@title}\\[24pt]
    By \\[24pt]
    \@author \\
    \vspace*{2.5in}
    A DISSERTATION \\[24pt]
    Submitted to \\
    Michigan State University \\
    in partial fulfillment of the requirements \\
    for the degree of \\[24pt]
    \MakeUppercase{\@degree} \\[24pt]
    \@department \\[24pt]
    \@copyrightyear 
  \end{center}
  \newpage
}
% The title page for a multivolume thesis is different.  This is the replace-
% ment titlepage.
\newcommand*{\volumetitlepage}{%
  \newpage
  \thispagestyle{empty}
  \begin{center}
   \vspace*{1in}
    \MakeUppercase{\@title}\\[24pt]
    VOLUME \Roman{volume}\\[24pt]
    By \\[24pt]
    \@author \\
    \vspace*{2.5in}
    A DISSERTATION \\[24pt]
    Submitted to \\
    Michigan State University \\
    in partial fulfillment of the requirements \\
    for the degree of \\[24pt]
    \MakeUppercase{\@degree} \\[24pt]
    \@department \\[24pt]
    \@copyrightyear 
  \end{center}
  \stepcounter{volume}
  \newpage
}

% This is the abstract.  Blah blah.  It should be limited to only two pages,
% but I do not check for that.  It's up to you to do that.
\newcommand*{\abstractpage}{%
  \newpage
  \thispagestyle{empty}
  \begin{center}
    ABSTRACT \\[24pt]
    \MakeUppercase{\@title}\\[24pt]
    By\\[24pt]
    \@author\\[24pt]
  \end{center}
  \doublespacing
  \@abstract
  \thispagestyle{empty}
  \newpage
}

% Copyright page.  I may alter it at a later date.
\newcommand*{\copyrightpage}{%
  \newpage
  \thispagestyle{empty}
  \vspace*{6in}
  \hspace*{2in}\parbox{3.5in}{
    {\Large
    Copyright by\\
    \MakeUppercase{\@author} \\
    \@copyrightyear }
  }
  \newpage
}

% Dedication.
\newcommand*{\dedicationpage}{%
  \newpage
  \parbox[c][0.9\textheight][c]{\linewidth}{
    \singlespacing
    \@dedication
    \doublespacing
  }
}

% Acknowledgments.  You should really include the following acknowledgment
% in your thesis by simply issuing a the '\classack' command.  It's defined 
% so you can insert it whereever you want.
\long\gdef\classack{Thanks to Christopher Waters for the \LaTeX{} class used to format this thesis.  }
\newcommand*{\acknowledgmentpage}{%
  \newpage
  \begin{center}
    ACKNOWLEDGMENTS \\[48pt]
  \end{center}
  \@acknowledgments
  \newpage
}

% I'm not sure what you'd put in the preface, but it's here, so feel free to 
% stick it in as well.
\newcommand*{\prefacepage}{%
  \newpage
  \begin{center}
    PREFACE \\[48pt]
  \end{center}
  \@preface
  \newpage
}

% Organize the table of contents.  This organizes the section and subsection
% headings to be indented slightly relative to the main chapter entry.  I'm
% not 100% happy with this code, but it seems to work correctly.

\renewcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
  \addpenalty{-\@highpenalty}%
  \vskip 1.0em \@plus\p@
  \setlength\@tempdima{1.5em}%
  \begingroup
  \parindent \z@ \rightskip \@pnumwidth
  \parfillskip -\@pnumwidth
  \leavevmode \bfseries
  \advance\leftskip\@tempdima
  \hskip -\leftskip
  #1\nobreak\dotfill \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
  \penalty\@highpenalty
  \endgroup
  \fi}

\renewcommand*\l@section{\vspace*{-15pt}\hspace*{0em}
  \@dottedtocline{1}{1.5em}{5em}}
\renewcommand*\l@subsection{\vspace*{-15pt}\hspace*{0em}
  \@dottedtocline{2}{3.0em}{5em}}
% Tables of contents/figures/tables/symbols.
\renewcommand*{\tableofcontents}{
  \newpage
  \begin{center}
    TABLE OF CONTENTS \\[48pt]
  \end{center}
  \singlespacing
  \@starttoc{toc}
  \newpage
}  
\renewcommand*{\listoffigures}{%
  \newpage
  \def\@tocpage{\roman{page}}
  \addtocontents{toc}{\protect\contentsline{chapter}{List of Figures}{\@tocpage}}
  \begin{center}
    LIST OF FIGURES \\[48pt]
  \end{center}
  \singlespacing
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{12pt} \fi
  \@starttoc{lof}
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{0pt} \fi
  \vspace*{24pt}
  \ifx \@colorfigures \undefined \relax \else \underline{\textbf{Images in this dissertation are presented in color.}} \fi
  \newpage
}
\renewcommand*{\listoftables}{%
  \newpage
  \def\@tocpage{\roman{page}}
  \addtocontents{toc}{\protect\contentsline{chapter}{List of Tables}{\@tocpage}}
  \begin{center}
    LIST OF TABLES \\[48pt]
  \end{center}
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{12pt} \fi
  \@starttoc{lot}
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{0pt} \fi
  \newpage
}
\newcommand*{\listofsymbols}{%
  \newpage
  \def\@tocpage{\roman{page}}
  \addtocontents{toc}{\protect\contentsline{chapter}{List of Symbols}{\@tocpage}}
  \begin{center}
    LIST OF SYMBOLS \\[48pt]
  \end{center}
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{12pt} \fi
  \@starttoc{los}
  \ifx \@finalstyle \undefined \relax \else \setlength{\parskip}{0pt} \fi
  \newpage
}
% This command is used to print the extra copies of the titlepage and abstract
% that are required to be turned in with the final draft.  I recommend doing 
% this before you do the \requirements command.
% I hate you, Grad School Bitches!
\newcommand*{\extratitlepage}{%
  \newpage
  \singlespacing
  \thispagestyle{empty}
  \begin{center}
   \vspace*{1in}
    \MakeUppercase{\@title}\\[24pt]
    By \\[24pt]
    \@author \\
    \vspace*{2.5in}
    AN ABSTRACT OF A DISSERTATION \\[24pt]
    Submitted to \\
    Michigan State University \\
    in partial fulfillment of the requirements \\
    for the degree of \\[24pt]
    \MakeUppercase{\@degree} \\[24pt]
    \@department \\[24pt]
    \@copyrightyear \\[24pt]
    \@advisorname
  \end{center}
  \newpage
}
\newcommand*{\extraabstractpage}{%
  \newpage
  \thispagestyle{empty}
  \begin{center}
    ABSTRACT \\[24pt]
    \MakeUppercase{\@title}\\[24pt]
    By\\[24pt]
    \@author\\[24pt]
  \end{center}
  \doublespacing
  \gdef\@evenhead{\hfill\@author}
  \@abstract
  \newpage
  \gdef\@evenhead{}
}

\newcommand{\extracopies}{
  \setlength{\headheight}{12pt}
  \setlength{\headsep}{4pt}
  \gdef\@oddfoot{\@empty}
  \gdef\@oddhead{\hfill\@author}
  \gdef\@evenfoot{\@empty}
  \gdef\@evenhead{\hfill\@author}
  \extratitlepage
  \extraabstractpage
  \setcounter{page}{1}
  \setlength{\headheight}{0pt}
  \setlength{\headsep}{0pt}
}

% Reset all the chapter and section headings to be smaller than the default
% book class.  If you don't like them, change them.  I like small caps, so
% that's what I used.
\gdef\@openright{0}
\gdef\@chapterimage{\empty}
\newcommand{\@makechapterimage}{%
  \begin{center}
    \includegraphics[]{\@chapterimage}
  \end{center}
}
\renewcommand\chapter{\clearpage
                    \thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\renewcommand{\@makechapterhead}[1]{%
    \vspace*{50\p@}%
  {\parindent \z@ \raggedright \normalfont
    \singlespacing
    \hrule                                        % horizontal line
    \vspace{5pt}%                                 % add vertical space
    \ifnum \c@secnumdepth >\m@ne
        \huge\scshape \@chapapp\space \thechapter: % Chapter number
        \par
        \nobreak
    \fi
    \interlinepenalty\@M
    \hangafter=1
    \hangindent=2cm
    \Huge \scshape #1\par                         % chapter title
    \vspace{5pt}%                                 % add vertical space
    \if \@chapterimage \empty \relax \else \@makechapterimage \fi
    \hrule                                        % horizontal rule
    \nobreak
    \vskip 36\p@
  }}
%% \renewcommand{\@makeschapterhead}[1]{%
%%   \vspace*{50\p@}%
%%   {\parindent \z@ \raggedright
%%     \normalfont
%%     \hrule                                        % horizontal line
%%     \vspace{5pt}%                                 % add vertical space
%%     \interlinepenalty\@M
%%     \Huge \scshape #1\par                         % chapter title
%%     \vspace{5pt}%                                 % add vertical space
%%     \hrule                                        % horizontal line
%%     \nobreak
%%     \vskip 40\p@
%%   }}
\renewcommand\section{\@startsection {section}{1}{\z@}%
  {-3.5ex \@plus -1ex \@minus -.2ex}%
  {2.3ex \@plus.2ex}%
  {\normalfont\Large\scshape}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\large\scshape}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\scshape}}
% Seriously, you shouldn't use paragraph and subparagraphs.
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
  {3.25ex \@plus1ex \@minus.2ex}%
  {-1em}%
  {\normalfont\normalsize\scshape}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
  {3.25ex \@plus1ex \@minus .2ex}%
  {-1em}%
  {\normalfont\normalsize\scshape}}

% Appendices have a cover page that is not in the default \appendix command.
% This fixes that.
\renewcommand\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \gdef\@chapapp{\appendixname}%
  \gdef\thechapter{\@Alph\c@chapter}
  \newpage
  \parbox[c][0.9\textheight]{0.9\linewidth}{
    \begin{center}
      APPENDICES
    \end{center}
  }
}

% The bibliography also has a cover page that isn't included by default.  This
% also ensures the bibliography gets included in the table of contents.  I have
% only tested it with bibitem, so I cannot guarantee that it will work with
% Bibtex.  
\renewcommand{\bibsection}{}
\renewenvironment{thebibliography}[1]{%
  \newpage
  \parbox[c][0.9\textheight]{0.9\linewidth}{
    \begin{center}
      REFERENCES
    \end{center}
  }
  \newpage
  \singlespacing
  \addtocontents{toc}{\protect\contentsline{chapter}{\numberline{}References}{\thepage}}
  \begin{center}
    REFERENCES \\[48pt]
  \end{center}
 \bibfont\bibsection\parindent \z@\list
   {\@biblabel{\arabic{NAT@ctr}}}{\@bibsetup{#1}%
    \setcounter{NAT@ctr}{0}}%
    \ifNAT@openbib
      \renewcommand\newblock{\par}
    \else
      \renewcommand\newblock{\hskip .11em \@plus.33em \@minus.07em}%
    \fi
    \sloppy\clubpenalty4000\widowpenalty4000
    \sfcode`\.=1000\relax
    \let\citeN\cite \let\shortcite\cite
    \let\citeasnoun\cite
 }{\def\@noitemerr{%
  \PackageWarning{natbib}
     {Empty `thebibliography' environment}}%
  \endlist\vskip-\lastskip}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% THESE ARE COMMANDS YOU MAY USE INSIDE YOUR THESIS DOCUMENT. %%%%%%%%
%%%%%%%%%%%% EVERYTHING BEFORE THIS IS A STYLE OR FORMATTING ISSUE. %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% These are the bibliographic codes from AASTeX.  I have copied them directly
% from the AASTeX source, which is Copyright (c) 2003 American Astronomical
% Society, and is available from http://www.journals.uchicago.edu/AAS/AASTeX/.
\let\jnl@style=\rmfamily 
\def\ref@jnl#1{{\jnl@style#1}}% 
\newcommand\aj{\ref@jnl{AJ}}%         % Astronomical Journal 
\newcommand\araa{\ref@jnl{ARA\&A}}%   % Annual Review of Astron and Astrophys 
\newcommand\apj{\ref@jnl{ApJ}}%       % Astrophysical Journal 
\newcommand\apjl{\ref@jnl{ApJ}}%      % Astrophysical Journal, Letters 
\newcommand\apjs{\ref@jnl{ApJS}}%     % Astrophysical Journal, Supplement 
\newcommand\ao{\ref@jnl{Appl.~Opt.}}% % Applied Optics 
\newcommand\apss{\ref@jnl{Ap\&SS}}%   % Astrophysics and Space Science 
\newcommand\aap{\ref@jnl{A\&A}}%      % Astronomy and Astrophysics 
\newcommand\aapr{\ref@jnl{A\&A~Rev.}}%% Astronomy and Astrophysics Reviews 
\newcommand\aaps{\ref@jnl{A\&AS}}%    % Astronomy and Astrophysics, Supplement 
\newcommand\azh{\ref@jnl{AZh}}%       % Astronomicheskii Zhurnal 
\newcommand\baas{\ref@jnl{BAAS}}%     % Bulletin of the AAS 
\newcommand\jrasc{\ref@jnl{JRASC}}%   % Journal of the RAS of Canada 
\newcommand\memras{\ref@jnl{MmRAS}}%  % Memoirs of the RAS 
\newcommand\mnras{\ref@jnl{MNRAS}}%   % Monthly Notices of the RAS 
\newcommand\pra{\ref@jnl{Phys.~Rev.~A}}%% Physical Review A: General Physics 
\newcommand\prb{\ref@jnl{Phys.~Rev.~B}}%% Physical Review B: Solid State 
\newcommand\prc{\ref@jnl{Phys.~Rev.~C}}%% Physical Review C 
\newcommand\prd{\ref@jnl{Phys.~Rev.~D}}%% Physical Review D 
\newcommand\pre{\ref@jnl{Phys.~Rev.~E}}%% Physical Review E 
\newcommand\prl{\ref@jnl{Phys.~Rev.~Lett.}}%% Physical Review Letters 
\newcommand\pasp{\ref@jnl{PASP}}%      % Publications of the ASP 
\newcommand\pasj{\ref@jnl{PASJ}}%      % Publications of the ASJ 
\newcommand\qjras{\ref@jnl{QJRAS}}%    % Quarterly Journal of the RAS 
\newcommand\skytel{\ref@jnl{S\&T}}%    % Sky and Telescope 
\newcommand\solphys{\ref@jnl{Sol.~Phys.}}%% Solar Physics 
\newcommand\sovast{\ref@jnl{Soviet~Ast.}}%% Soviet Astronomy 
\newcommand\ssr{\ref@jnl{Space~Sci.~Rev.}}%% Space Science Reviews 
\newcommand\zap{\ref@jnl{ZAp}}%        % Zeitschrift fuer Astrophysik 
\newcommand\nat{\ref@jnl{Nature}}%     % Nature 
\newcommand\iaucirc{\ref@jnl{IAU~Circ.}}%% IAU Cirulars 
\newcommand\aplett{\ref@jnl{Astrophys.~Lett.}}%% Astrophysics Letters 
\newcommand\apspr{\ref@jnl{Astrophys.~Space~Phys.~Res.}}%% Astrophysics Space Physics Research 
\newcommand\bain{\ref@jnl{Bull.~Astron.~Inst.~Netherlands}}%% Bulletin Astronomical Institute of the Netherlands 
\newcommand\fcp{\ref@jnl{Fund.~Cosmic~Phys.}}%% Fundamental Cosmic Physics 
\newcommand\gca{\ref@jnl{Geochim.~Cosmochim.~Acta}}%% Geochimica Cosmochimica Acta 
\newcommand\grl{\ref@jnl{Geophys.~Res.~Lett.}}%% Geophysics Research Letters 
\newcommand\jcp{\ref@jnl{J.~Chem.~Phys.}}%% Journal of Chemical Physics 
\newcommand\jgr{\ref@jnl{J.~Geophys.~Res.}}%% Journal of Geophysics Research 
\newcommand\jqsrt{\ref@jnl{J.~Quant.~Spec.~Radiat.~Transf.}}%% Journal of Quantitiative Spectroscopy and Radiative Trasfer 
\newcommand\memsai{\ref@jnl{Mem.~Soc.~Astron.~Italiana}}%% Mem. Societa Astronomica Italiana 
\newcommand\nphysa{\ref@jnl{Nucl.~Phys.~A}}%% Nuclear Physics A 
\newcommand\physrep{\ref@jnl{Phys.~Rep.}}%% Physics Reports 
\newcommand\physscr{\ref@jnl{Phys.~Scr}}% % Physica Scripta 
\newcommand\planss{\ref@jnl{Planet.~Space~Sci.}}%% Planetary Space Science 
\newcommand\procspie{\ref@jnl{Proc.~SPIE}}%% Proceedings of the SPIE 
\let\astap=\aap 
\let\apjlett=\apjl 
\let\apjsupp=\apjs 
\let\applopt=\ao 
% End bibliographic codes.

% I stole this from aastex.  Hopefully it'll work.
\newcommand\anchor[2]{#2}% 
\newcommand\url{\@dblarg\@url}% 
\def\@url[#1]{\anchor{#1}}% 


% These are a few math commands that did not exist.  They do now
\newcommand{\erf}{\mathop{\rm erf}\nolimits} 
\newcommand{\farcs}{\mbox{\ensuremath{.\!\!^{\prime\prime}}}}
\newcommand{\EE}[1]{%
  \ensuremath{\times 10^{#1}}}
% This command actually formats the beginning of the thesis to match the 
% requirements.  The comments included are to help anyone else who edits
% this in the future.
\newcommand{\requirements}{%
  %Set the footer to be lowercase roman, since that is the requirement.
  \gdef\@oddfoot{\hfill\roman{page}\hfill}
  \gdef\@oddhead{\@empty}
  \gdef\@evenfoot{\hfill\roman{page}\hfill}
  \gdef\@evenhead{\@empty}
  % The bookplate doesn't count in the page count.
  \setcounter{page}{0}
  \ifx \@finalstyle \undefined \relax \else \bookplate \fi
  \ifx \@finalstyle \undefined \relax \else \extracopies{} \fi
  %Set the footer to be lowercase roman, since that is the requirement.
  \gdef\@oddfoot{\hfill\roman{page}\hfill}
  \gdef\@oddhead{\@empty}
  \gdef\@evenfoot{\hfill\roman{page}\hfill}
  \gdef\@evenhead{\@empty}
  % Check if we're multivolume, and then use the correct titlepage
  \ifx \@multivolume \undefined \titlepage \else \volumetitlepage \fi
  % The titlepage also doesn't count as a page.
  %  \setcounter{page}{1}
  % Ok, so the titlepage apparently does count as a page.
  % You need an abstract.
  \abstractpage
  % But the rest of the stuff isn't technically required.
  \ifx \@copyrightyear \undefined \relax \else \copyrightpage \fi
  \ifx \@dedication \undefined \relax \else \dedicationpage \fi
  \ifx \@acknowledgments \undefined \relax \else \acknowledgmentpage \fi
  \ifx \@preface \undefined \relax \else \prefacepage \fi

  % Print out the lists.
  \tableofcontents
  \listoftables
  \listoffigures
  \ifx \@uselos \undefined \relax \else \listofsymbols \fi
  % Reset the page counter, and then reset the footer to be in Arabic numerals
  \setcounter{page}{1}
  \gdef\@oddfoot{\hfill\arabic{page}\hfill}
  \gdef\@oddhead{\@empty}
  \gdef\@evenfoot{\hfill\arabic{page}\hfill}
  \gdef\@evenhead{\@empty}
  \ifx \@draftstyle \undefined \doublespacing \else \singlespacing \fi
  \raggedbottom
}

% This command tells Latex to break the document into a new volume.  It 
% increments the volume counter, and prints the new titlepage.
\newcommand{\newvolume}{%
  \singlespacing
  \volumetitlepage
  \ifx \@draftstyle \undefined \doublespacing \else \singlespacing \fi
}

% This defines a symbol.  It does not print anything out.  Call it as:
% \definesymbol{SYMBOL}{Definition}
% Take care to properly ensure math mode.
\newcommand*{\allowlos}[1]{\gdef\@uselos{#1}}
\newcommand{\definesymbol}[2]{\ensuremath{#1\!}\addtocontents{los}{%
    \protect\contentsline{symbol}{
      \protect\numberline{\ensuremath{#1}}#2}{\thepage}}
}
\newcommand{\invisiblesymbol}[2]{\addtocontents{los}{%
    \protect\contentsline{symbol}{
      \protect\numberline{$#1$}#2}{\thepage}}
}
\let\l@symbol\l@figure

% This allows a quick way to insert a simple figure.  Also, you can set 
% scaling and whatnot here, too.
\newcommand*{\thesisfigure}[6]{%
  \def\@image{#1}
  \def\@rotation{#2}
  \def\@scale{#3}
  \def\@lcaption{#4}
  \def\@scaption{#5}
  \def\@label{#6}
  \def\@autoscale{0.0}
  \ifx\@rotation\empty\def\@rotation{0.0} \fi
  \ifx\@scale\empty\def\@scale{1.0} \fi
  \begin{figure}[ht]
    \begin{center}
      \ifx\@image\empty\relax\else
      \ifx\@scale\@autoscale
      \includegraphics[angle=\@rotation,width=1.0\textwidth,clip]{#1}
      \else 
      \includegraphics[angle=\@rotation,scale=\@scale,clip]{#1}
      \fi
      \fi
    \end{center}
    \ifx\@scaption\empty\caption{#4}\else\caption[#5]{#4} \fi
    \ifx\@label\empty\relax\else\label{#6}\fi
  \end{figure}
}

% Commands to make long tables work reasonably.  This was the hardest part of 
% the entire fucking thing.  I hate how hard simple things end up.
\newenvironment{thesistable}[1]{%
  \begin{small}
    \begin{center}
      \begin{longtable}{#1}
}{%
  \end{longtable}
    \end{center}
    \end{small}
}
\newenvironment{rotthesistable}[1]{%
  \def\@spec{#1}
  \begin{landscape}
    \begin{thesistable}{\@spec}
}{%
  \end{thesistable}
    \end{landscape}
}
\newcommand{\centercolumn}[1]{%
  \multicolumn{1}{c}{#1}
}
\newcommand{\thesistablehead}[4]{%
  \caption[#2]{#1\label{#4}} \\[0pt]
  \hline
  \hline
  #3 \rule{0pt}{14pt}\\[4pt]
  \hline
  \endfirsthead
  \caption[]{#2 (continued)} \\[0pt]
  \hline
  \hline
  #3 \rule{0pt}{14pt}\\[4pt]
  \hline
  \endhead
  \hline
  \endfoot
}


% I kind of want images on each of my chapter titles.  For funsies.
\newcommand{\imchapter}[2][\empty]{%
  \gdef\@chapterimage{#1}
  \chapter{#2}
  \gdef\@chapterimage{\empty}
}

% Here is what is currently not supported:
% 1.   Large figures are supposed to use a 'facing page.'  I do not do this,
%      because it requires some magic.
% 2.   I have not written a reprint coverpage, because I don't think reprints
%      belong in a thesis.  It's just as easy to rewrite it.
% 3.   The list of symbols is not very robust.  It does not alphabetize, nor
%      does it divide into chapters.  I'm not sure I'd recommend using it.
%      I attempted to make it automatic, but I can't get it to switch properly.
%      I'm done for today.

% I have also included a 'minimal' thesis form, so you can see what needs to 
% be in the final document:
% \documentclass{msuthesis}
% \title{Thesis Title}
% \author{Author Name}
% \multivolume                  %If the thesis has more than one volume
% \subject{Astrophysics}
% \degree{Doctor of Philosophy}
% \department{Department of Physics and Astronomy}
% \abstract{Text of Abstract goes here.}
% \dedication{Dedication Text}
% \copyrightyear{2006}
% \acknowledgments{\classack}
% \preface{Preface Text}
%
% \begin{document}
% \extracopies                  % Generate extra title page and abstract
% \requirements
%
% \chapter{Chapter 1}
% TEXT
% \newvolume                    % To break up the volumes.
% \chapter{Chapter 2}
% MORE TEXT
% \appendix
% \chapter{Appendix A}
% STILL MORE TEXT
% \makefigure{/tmp/blah.eps}{270}{0.5}{Full Caption Here}{Optional Short Caption}{fig: blah}
% \begin{thebibliography}{asdfadf}
%   \bibitem[Ref Text]{refkey} Reference Citation
% \end{thebibliography}
% \end{document}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  STUFF NOT COMPLETELY RELATED TO THE THESIS CLASS  %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Sizes of past Astronomy Theses 
% Year Author                    Advisor Pages
% 2007 Brian Marsteller          Beers   
% 2005 Michelle Ouellette        Brown   124
% 2004 Karen Kinemuchi           Smith   218
% 2004 Regner Trampedach         Stein   209
% 2003 Brian Sharpee             Baldwin 309
% 2003 Dali Giorgobiani          Stein   106
% 2002 Mike Davis                Loh     66
% 2002 Dave Bercik               Stein   131
% 2000 Bart Pritzl               Smith   167
% 2000 Jaehyon Rhee              Beers   131
% 1995 Ron Wilhelm               Beers   350(2v)
% 1994 Nancy Silbermann          Smith   224

